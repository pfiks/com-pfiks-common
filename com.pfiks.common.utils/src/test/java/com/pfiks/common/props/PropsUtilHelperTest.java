/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.props;

import com.liferay.portal.kernel.util.PropsUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mockStatic;

@RunWith(MockitoJUnitRunner.class)
public class PropsUtilHelperTest {

	private static final String COMPANY_WEB_ID = "teamworxx.net";
	private static final String OTHER_COMPANY_WEB_ID = "khub.net";

	private PropsUtilHelper propsUtilHelper;


	@Before
	public void setUp() throws Exception {
		propsUtilHelper = new PropsUtilHelper();
	}

	@Test
	public void canCustomiseCompany_whenDefaultCompanyAdminOnlyTrueCompanyIsDefault_returnsFalse() {

		try (MockedStatic<PropsUtil> propsUtil = mockStatic(PropsUtil.class)) {

			propsUtil.when(() -> PropsUtil.get("company.default.web.id")).thenReturn(COMPANY_WEB_ID);
			propsUtil.when(() -> PropsUtil.get("default.company.admin.only")).thenReturn("true");

			assertFalse(propsUtilHelper.canCustomiseCompany(COMPANY_WEB_ID));

		}

	}

	@Test
	public void canCustomiseCompany_whenDefaultCompanyAdminOnlyTrueCompanyIsNotDefault_returnsTrue() {

		try (MockedStatic<PropsUtil> propsUtil = mockStatic(PropsUtil.class)) {

			propsUtil.when(() -> PropsUtil.get("company.default.web.id")).thenReturn(COMPANY_WEB_ID);
			propsUtil.when(() -> PropsUtil.get("default.company.admin.only")).thenReturn("true");

			assertTrue(propsUtilHelper.canCustomiseCompany(OTHER_COMPANY_WEB_ID));

		}

	}

	@Test
	public void canCustomiseCompany_whenDefaultCompanyAdminOnlyFalseCompanyIsDefault_returnsTrue() {

		try (MockedStatic<PropsUtil> propsUtil = mockStatic(PropsUtil.class)) {

			propsUtil.when(() -> PropsUtil.get("company.default.web.id")).thenReturn(COMPANY_WEB_ID);
			propsUtil.when(() -> PropsUtil.get("default.company.admin.only")).thenReturn("false");

			assertTrue(propsUtilHelper.canCustomiseCompany(COMPANY_WEB_ID));

		}

	}

	@Test
	public void canCustomiseCompany_whenDefaultCompanyAdminOnlyFalseCompanyIsNotDefault_returnsTrue() {

		try (MockedStatic<PropsUtil> propsUtil = Mockito.mockStatic(PropsUtil.class)) {

			propsUtil.when(() -> PropsUtil.get("company.default.web.id")).thenReturn(COMPANY_WEB_ID);
			propsUtil.when(() -> PropsUtil.get("default.company.admin.only")).thenReturn("false");

			assertTrue(propsUtilHelper.canCustomiseCompany(OTHER_COMPANY_WEB_ID));

		}

	}

}
