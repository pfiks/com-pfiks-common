/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.servicecontext;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.arrayContaining;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertTrue;

import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.workflow.WorkflowConstants;

import org.junit.Test;

public class ServiceContextHelperTest {

	private static final long CATEGORY_ID_1 = 1L;

	private static final long CATEGORY_ID_2 = 2L;

	private static final long CATEGORY_ID_3 = 3L;

	private static final long[] CATEGORY_IDS = new long[]{CATEGORY_ID_1, CATEGORY_ID_2, CATEGORY_ID_3};

	private static long COMPANY_ID = 4L;

	private static final long GROUP_ID = 5L;

	private static final long USER_ID = 6L;

	private ServiceContextHelper serviceContextHelper = new ServiceContextHelper();

	@Test
	public void getServiceContext_WhenCompanyIdIsGiven_ThenNewInstanceIsCreatedWithTheCompanyId() {

		ServiceContext serviceContext = serviceContextHelper.getServiceContext(COMPANY_ID);

		assertThat(serviceContext.getCompanyId(), equalTo(COMPANY_ID));

	}

	@Test
	public void getServiceContext_WhenCompanyIdAndCategoryIdsAreGiven_ThenNewInstanceIsCreatedWithGivenParams() {

		ServiceContext serviceContext = serviceContextHelper.getServiceContext(COMPANY_ID, CATEGORY_IDS);

		assertThat(serviceContext.getCompanyId(), equalTo(COMPANY_ID));
		assertThat(ArrayUtil.toArray(serviceContext.getAssetCategoryIds()), arrayContaining(CATEGORY_ID_1, CATEGORY_ID_2, CATEGORY_ID_3));

	}

	@Test
	public void getServiceContext_WhenCompanyIdAndGroupIdAndUserIdAreGiven_ThenNewInstanceIsCreatedWithGivenParams() {

		ServiceContext serviceContext = serviceContextHelper.getServiceContext(COMPANY_ID, GROUP_ID, USER_ID);

		assertThat(serviceContext.getCompanyId(), equalTo(COMPANY_ID));
		assertThat(serviceContext.getScopeGroupId(), equalTo(GROUP_ID));
		assertThat(serviceContext.getUserId(), equalTo(USER_ID));
		assertTrue(serviceContext.isAddGroupPermissions());
		assertTrue(serviceContext.isAddGuestPermissions());
		assertThat(serviceContext.getWorkflowAction(), equalTo(WorkflowConstants.ACTION_PUBLISH));

	}

	@Test
	public void getServiceContext_WhenCompanyIdAndGroupIdAndUserIdAndCategoryIdsAreGiven_ThenNewInstanceIsCreatedWithGivenParams() {

		ServiceContext serviceContext = serviceContextHelper.getServiceContext(COMPANY_ID, GROUP_ID, USER_ID, CATEGORY_IDS);

		assertThat(serviceContext.getCompanyId(), equalTo(COMPANY_ID));
		assertThat(serviceContext.getScopeGroupId(), equalTo(GROUP_ID));
		assertThat(serviceContext.getUserId(), equalTo(USER_ID));
		assertTrue(serviceContext.isAddGroupPermissions());
		assertTrue(serviceContext.isAddGuestPermissions());
		assertThat(serviceContext.getWorkflowAction(), equalTo(WorkflowConstants.ACTION_PUBLISH));
		assertThat(ArrayUtil.toArray(serviceContext.getAssetCategoryIds()), arrayContaining(CATEGORY_ID_1, CATEGORY_ID_2, CATEGORY_ID_3));

	}

}
