/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.portal;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.PortalUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mockStatic;

@RunWith(MockitoJUnitRunner.class)
public class PortalUtilHelperTest {

	private PortalUtilHelper portalUtilHelper;

	@Mock
	private User mockUser;

	private MockedStatic<PortalUtil> mockPortalUtil;

	@Before
	public void setUp() throws Exception {

		mockPortalUtil = mockStatic(PortalUtil.class);

		portalUtilHelper = new PortalUtilHelper();

	}

	@After
	public void teardown() {
		mockPortalUtil.close();
	}

	@Test
	public void isCompanyAdmin_WhenException_ThenReturnsFalse() throws Exception {

		mockPortalUtil.when(() -> PortalUtil.isCompanyAdmin(mockUser)).thenThrow(new PortalException());

		boolean result = portalUtilHelper.isCompanyAdmin(mockUser);

		assertThat(result, equalTo(false));

	}

	@Test
	public void isCompanyAdmin_WhenUserIsAdmin_ThenReturnsTrue() throws Exception {

		mockPortalUtil.when(() -> PortalUtil.isCompanyAdmin(mockUser)).thenReturn(true);

		boolean result = portalUtilHelper.isCompanyAdmin(mockUser);

		assertThat(result, equalTo(true));

	}

	@Test
	public void isCompanyAdmin_WhenUserIsNotAdmin_ThenReturnsFalse() throws Exception {

		mockPortalUtil.when(() -> PortalUtil.isCompanyAdmin(mockUser)).thenReturn(false);

		boolean result = portalUtilHelper.isCompanyAdmin(mockUser);

		assertThat(result, equalTo(false));


	}

}
