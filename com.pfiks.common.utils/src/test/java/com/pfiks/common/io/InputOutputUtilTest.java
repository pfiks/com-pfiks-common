/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.io;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.pfiks.common.exception.CommonUtilsException;
import com.pfiks.common.internal.CommonUtilsService;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InputOutputUtilTest {

	private InputOuputUtil inputOutputUtil;

	private final static String ORIGINAL_STRING = "<root available-locales=\"[$AVAILABLE_LOCALES$]\" default-locale=\"[$LOCALE_DEFAULT$]\" />";

	private final static String TRANSLATED_STRING = "<root available-locales=\"en_GB,en_US\" default-locale=\"en_GB\" />";

	@Mock
	private CommonUtilsService mockCommonUtilsService;

	@Mock
	private Group mockGroup;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Before
	public void setUp() {

		inputOutputUtil = new InputOuputUtil();
		inputOutputUtil.setCommonUtilsService(mockCommonUtilsService);

	}

	@Test(expected = CommonUtilsException.class)
	public void getStringFromInputStream_WhenInputStreamIsNull_ThenThrowsCommonUtilsExceptions() throws Exception {
		inputOutputUtil.getStringFromInputStream(null);
	}

	@Test
	public void getStringFromInputStream_WhenInputStreamIsEmtpy_ThenAnEmptyStringIsReturned() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("EmptyFile.json");

		String stringFromInputStream = inputOutputUtil.getStringFromInputStream(inputStreamFile);

		assertThat(stringFromInputStream, isEmptyString());
	}

	@Test
	public void getStringFromInputStream_WhenInputStreamHasContent_ThenTheContentOfTheInputStreamIsReturnedAsString() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("FileWithStringContent.json");

		String stringFromInputStream = inputOutputUtil.getStringFromInputStream(inputStreamFile);

		assertThat(stringFromInputStream, is("Hello Test Content"));
	}

	@Test(expected = CommonUtilsException.class)
	public void getLocalisedStringFromInputStream_WhenExceptionGettingInputStream_ThenThrowsCommonUtilsException() throws Exception {
		inputOutputUtil.getLocalisedStringFromInputStream(null, null, null);
	}

	@Test
	public void getLocalisedStringFromInputStream_WhenInputStreamIsEmtpyAndLanguageIdAndAvailableLanguagesNotSet_ThenAnEmptyStringIsReturned() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("EmptyFile.json");
		String languageId = StringPool.BLANK;
		String availableLanguages = StringPool.BLANK;

		String stringFromInputStream = inputOutputUtil.getLocalisedStringFromInputStream(inputStreamFile, languageId, availableLanguages);

		assertThat(stringFromInputStream, isEmptyString());
	}

	@Test
	public void getLocalisedStringFromInputStream_WhenInputStreamIsEmtpyAndLanguageIdAndAvailableLanguagesSet_ThenAnEmptyStringIsReturned() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("EmptyFile.json");
		String languageId = "en_GB";
		String availableLanguages = "en_GB,en_US";

		String stringFromInputStream = inputOutputUtil.getLocalisedStringFromInputStream(inputStreamFile, languageId, availableLanguages);

		assertThat(stringFromInputStream, isEmptyString());
	}

	@Test
	public void getLocalisedStringFromInputStream_WhenLanguageIdAndAvailableLanguagesSet_ThenTheContentOfTheInputStreamIsReturnedAsString() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("FileWithContentAndLanguages.xml");
		String languageId = "en_GB";
		String availableLanguages = "en_GB,en_US";

		String stringFromInputStream = inputOutputUtil.getLocalisedStringFromInputStream(inputStreamFile, languageId, availableLanguages);

		assertThat(stringFromInputStream, is(TRANSLATED_STRING));
	}

	@Test
	public void getLocalisedStringFromInputStream_WhenLanguageIdNotSetAndAvailableLanguagesSet_ThenTheContentOfTheInputStreamIsReturnedWithoutLanguageIdsReplaced() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("FileWithContentAndLanguages.xml");
		String languageId = StringPool.BLANK;
		String availableLanguages = "en_GB,en_US";

		String stringFromInputStream = inputOutputUtil.getLocalisedStringFromInputStream(inputStreamFile, languageId, availableLanguages);

		assertThat(stringFromInputStream, is(ORIGINAL_STRING));
	}

	@Test
	public void getLocalisedStringFromInputStream_WhenLanguageIdSetAndAvailableLanguagesNotSet_ThenTheContentOfTheInputStreamIsReturnedWithoutLanguageIdsReplaced() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("FileWithContentAndLanguages.xml");
		String languageId = "en_GB";
		String availableLanguages = StringPool.BLANK;

		String stringFromInputStream = inputOutputUtil.getLocalisedStringFromInputStream(inputStreamFile, languageId, availableLanguages);

		assertThat(stringFromInputStream, is(ORIGINAL_STRING));
	}

	@Test(expected = CommonUtilsException.class)
	public void getLocalizedContentFromInputStream_WhenExceptionGettingInputStream_ThenThrowsCommonUtilsException() throws Exception {
		inputOutputUtil.getLocalizedContentFromInputStream(mockGroup, null);
	}

	@Test
	public void getLocalizedContentFromInputStream_WhenInputStreamIsEmtpyAndLanguageIdAndAvailableLanguagesNotSet_ThenAnEmptyStringIsReturned() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("EmptyFile.json");
		String languageId = StringPool.BLANK;
		String availableLanguages = StringPool.BLANK;
		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(mockCommonUtilsService.getLanguageId(mockGroup, mockUnicodeProperties)).thenReturn(languageId);
		when(mockCommonUtilsService.getAvailableLocales(mockGroup, mockUnicodeProperties)).thenReturn(availableLanguages);

		String result = inputOutputUtil.getLocalizedContentFromInputStream(mockGroup, inputStreamFile);

		assertThat(result, isEmptyString());
	}

	@Test
	public void getLocalizedContentFromInputStream_WhenInputStreamIsEmtpyAndLanguageIdAndAvailableLanguagesSet_ThenAnEmptyStringIsReturned() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("EmptyFile.json");
		String languageId = "en_GB";
		String availableLanguages = "en_GB,en_US";
		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(mockCommonUtilsService.getLanguageId(mockGroup, mockUnicodeProperties)).thenReturn(languageId);
		when(mockCommonUtilsService.getAvailableLocales(mockGroup, mockUnicodeProperties)).thenReturn(availableLanguages);

		String result = inputOutputUtil.getLocalizedContentFromInputStream(mockGroup, inputStreamFile);

		assertThat(result, isEmptyString());
	}

	@Test
	public void getLocalizedContentFromInputStream_WhenLanguageIdAndAvailableLanguagesSet_ThenTheContentOfTheInputStreamIsReturnedAsString() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("FileWithContentAndLanguages.xml");
		String languageId = "en_GB";
		String availableLanguages = "en_GB,en_US";
		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(mockCommonUtilsService.getLanguageId(mockGroup, mockUnicodeProperties)).thenReturn(languageId);
		when(mockCommonUtilsService.getAvailableLocales(mockGroup, mockUnicodeProperties)).thenReturn(availableLanguages);

		String result = inputOutputUtil.getLocalizedContentFromInputStream(mockGroup, inputStreamFile);

		assertThat(result, is(TRANSLATED_STRING));
	}

	@Test
	public void getLocalizedContentFromInputStream_WhenLanguageIdNotSetAndAvailableLanguagesSet_ThenTheContentOfTheInputStreamIsReturnedWithoutLanguageIdsReplaced() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("FileWithContentAndLanguages.xml");
		String languageId = StringPool.BLANK;
		String availableLanguages = "en_GB,en_US";
		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(mockCommonUtilsService.getLanguageId(mockGroup, mockUnicodeProperties)).thenReturn(languageId);
		when(mockCommonUtilsService.getAvailableLocales(mockGroup, mockUnicodeProperties)).thenReturn(availableLanguages);

		String result = inputOutputUtil.getLocalizedContentFromInputStream(mockGroup, inputStreamFile);

		assertThat(result, is(ORIGINAL_STRING));
	}

	@Test
	public void getLocalizedContentFromInputStream_WhenLanguageIdSetAndAvailableLanguagesNotSet_ThenTheContentOfTheInputStreamIsReturnedWithoutLanguageIdsReplaced() throws Exception {
		InputStream inputStreamFile = getInputStreamFile("FileWithContentAndLanguages.xml");
		String languageId = "en_GB";
		String availableLanguages = StringPool.BLANK;
		when(mockGroup.getTypeSettingsProperties()).thenReturn(mockUnicodeProperties);
		when(mockCommonUtilsService.getLanguageId(mockGroup, mockUnicodeProperties)).thenReturn(languageId);
		when(mockCommonUtilsService.getAvailableLocales(mockGroup, mockUnicodeProperties)).thenReturn(availableLanguages);

		String result = inputOutputUtil.getLocalizedContentFromInputStream(mockGroup, inputStreamFile);

		assertThat(result, is(ORIGINAL_STRING));
	}

	@Test
	public void getKeyFromFileName_WhenFileIsBlank_ThenReturnsEmptyString() {
		assertThat(inputOutputUtil.getKeyFromFileName(""), is(""));
	}

	@Test
	public void getKeyFromFileName_WhenNoError_ThenTheReturnedKeyIsAlwaysUppercasedAndWithDashesInsteadOfSpaces() {
		assertThat(inputOutputUtil.getKeyFromFileName("My File Name"), equalTo("MY-FILE-NAME"));
		assertThat(inputOutputUtil.getKeyFromFileName("My_File-name"), equalTo("MY_FILE-NAME"));
		assertThat(inputOutputUtil.getKeyFromFileName("myFileName"), equalTo("MYFILENAME"));
	}

	@Test
	public void getCleanFileNameFromFilePath_WhenNoError_ThenReturnsTheClearedFileName() {
		assertThat(inputOutputUtil.getCleanFileNameFromFilePath("one/two.com/three.com.it"), equalTo("three.com"));
		assertThat(inputOutputUtil.getCleanFileNameFromFilePath("one/two.com/three.com"), equalTo("three"));
		assertThat(inputOutputUtil.getCleanFileNameFromFilePath("one"), equalTo("one"));
		assertThat(inputOutputUtil.getCleanFileNameFromFilePath("one/two"), equalTo("two"));
	}

	private InputStream getInputStreamFile(final String fileName) throws FileNotFoundException {
		final URL resource = Thread.currentThread().getContextClassLoader().getResource(fileName);
		return new FileInputStream(resource.getPath());
	}

}