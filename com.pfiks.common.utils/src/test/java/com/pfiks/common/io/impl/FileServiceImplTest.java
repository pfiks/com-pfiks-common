/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.io.impl;

import com.pfiks.common.io.InputOuputUtil;
import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FileServiceImplTest {

	private FileServiceImpl fileServiceImpl;

	@Mock
	private Bundle mockBundle;

	private MockedStatic<FrameworkUtil> mockFrameworkUtil;

	@Mock
	private InputOuputUtil mockInputOutputUtil;

	@Before
	public void setup() {

		mockFrameworkUtil = mockStatic(FrameworkUtil.class);

		fileServiceImpl = new FileServiceImpl();
		fileServiceImpl.setInputOutputUtil(mockInputOutputUtil);

	}

	@After
	public void teardown() {
		mockFrameworkUtil.close();
	}

	@Test
	public void loadFile_WhenFileIsFound_ThenTheInputStreamIsReturned() throws IOException {

		InputStream loadFile = fileServiceImpl.loadFile(getClass(), "FileWithStringContent.json");

		String fileContent = IOUtils.toString(loadFile, "UTF8");

		assertThat(fileContent, equalTo("Hello Test Content"));

	}

	@Test
	public void loadFile_WhenNoFileIsFound_ThenReturnsNull() throws IOException {

		InputStream loadFile = fileServiceImpl.loadFile(getClass(), "unknownfile.xml");

		assertThat(loadFile, nullValue());

	}

	@Test
	public void loadFiles_WhenFilesAreFound_ThenReturnsMapWithEvaluatedFileNameAndTheInputStreamForEachFileFound() throws Exception {

		String path = "directory";
		String extension = "*.json";
		String firstFileName = "firstFileName";
		String secondFileName = "secondFileName";
		URL firstFileURL = new File("directory/firstFileName.json").toURI().toURL();
		URL secondFileURL = new File("directory/secondFileName.json").toURI().toURL();

		List<URL> fileURLs = new ArrayList<>();
		fileURLs.add(firstFileURL);
		fileURLs.add(secondFileURL);

		when(mockInputOutputUtil.getCleanFileNameFromFilePath(firstFileURL.getPath())).thenReturn(firstFileName);
		when(mockInputOutputUtil.getCleanFileNameFromFilePath(secondFileURL.getPath())).thenReturn(secondFileName);
		when(mockBundle.findEntries(path, extension, true)).thenReturn(Collections.enumeration(fileURLs));

		mockFrameworkUtil.when(() -> FrameworkUtil.getBundle(any(Class.class))).thenReturn(mockBundle);
		Map<String, InputStream> inputStreams = fileServiceImpl.loadFiles(FileServiceImpl.class, path, extension);

		assertThat(inputStreams.keySet(), containsInAnyOrder(firstFileName, secondFileName));

	}

	@Test
	public void loadFiles_WhenNoFilesAreFound_ThenReturnsEmptyMap() {

		mockFrameworkUtil.when(() -> FrameworkUtil.getBundle(any(Class.class))).thenReturn(mockBundle);
		Map<String, InputStream> inputStreams = fileServiceImpl.loadFiles(FileServiceImpl.class, "directory", "*.json");

		assertThat(inputStreams.size(), is(0));

	}

}
