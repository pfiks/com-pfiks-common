/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.time;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.pfiks.common.locale.LocaleMapUtil;
import com.pfiks.common.time.constants.DatePatterns;

public class DateFormatterTest extends Mockito {

	private static final String TIMEZONE_ID_EUROPE_LONDON = "Europe/London";
	private static final String TIMEZONE_ID_UTC = "UTC";
	private static final String TIMEZONE_ID_AFRICA_CAIRO = "Africa/Cairo";
	private static final long MILLIS_year2013_month12_day22_hour17_min42_sec8_milli33 = 1387734128033L;
	private static final Date DATE_year2013_month12_day22_hour17_min42_sec8_milli33 = new Date(MILLIS_year2013_month12_day22_hour17_min42_sec8_milli33);
	private static final Instant INSTANT_year2013_month12_day22_hour17_min42_sec8_milli33 = DATE_year2013_month12_day22_hour17_min42_sec8_milli33.toInstant();
	private static final ZoneId ZONE_ID_AFRICA_CAIRO = ZoneId.of(TIMEZONE_ID_AFRICA_CAIRO);
	private Locale locale = Locale.ENGLISH;

	private DateFormatter dateFormatter = new DateFormatter();
	private LocaleMapUtil localeMapUtil = new LocaleMapUtil();

	@Before
	public void setUp() {
		dateFormatter.setLocaleMapUtil(localeMapUtil);
	}

	@Test
	public void getFormattedDate_WithPattern_dd_MM_yyyy_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedDate(DATE_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_UTC, "dd MM yyyy");
		assertThat(formattedDate, is("22 12 2013"));
	}

	@Test
	public void getFormattedDate_WithPattern_yyyy_MM_dd_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedDate(DATE_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_UTC, "yyyy MM dd");
		assertThat(formattedDate, is("2013 12 22"));
	}

	@Test
	public void getFormattedDate_WithEuropeLondon_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedDate(DATE_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_EUROPE_LONDON, "yyyy MM dd");
		assertThat(formattedDate, is("2013 12 22"));
	}

	@Test
	public void getFormattedDate_WithUTC_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedDate(DATE_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_UTC, "yyyy MM dd HH:mm:ss");
		assertThat(formattedDate, is("2013 12 22 17:42:08"));
	}

	@Test
	public void getFormattedDate_WithUTC_ShouldReturnFormattedDate_WithHour() {
		String formattedDate = dateFormatter.getFormattedDate(DATE_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_UTC, "yyyy MM dd hh:mm:ss");
		assertThat(formattedDate, is("2013 12 22 05:42:08"));
	}

	@Test
	public void getFormattedDate_WithAfricaCairo_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedDate(DATE_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_AFRICA_CAIRO, "yyyy MM dd HH:mm:ss");
		// Cairo is +2h so h17 is h19
		assertThat(formattedDate, is("2013 12 22 19:42:08"));
	}

	@Test
	public void getFormattedInstant_WithPattern_dd_MM_yyyy_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedInstant(INSTANT_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_UTC, "dd MM yyyy");
		assertThat(formattedDate, is("22 12 2013"));
	}

	@Test
	public void getFormattedInstant_WithPattern_yyyy_MM_dd_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedInstant(INSTANT_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_UTC, "yyyy MM dd");
		assertThat(formattedDate, is("2013 12 22"));
	}

	@Test
	public void getFormattedInstant_WithEuropeLondon_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedInstant(INSTANT_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_EUROPE_LONDON, "yyyy MM dd");
		assertThat(formattedDate, is("2013 12 22"));
	}

	@Test
	public void getFormattedInstant_WithUTC_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedInstant(INSTANT_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_UTC, "yyyy MM dd HH:mm:ss");
		assertThat(formattedDate, is("2013 12 22 17:42:08"));
	}

	@Test
	public void getFormattedInstant_WithUTC_ShouldReturnFormattedDate_WithHour() {
		String formattedDate = dateFormatter.getFormattedInstant(INSTANT_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_UTC, "yyyy MM dd hh:mm:ss");
		assertThat(formattedDate, is("2013 12 22 05:42:08"));
	}

	@Test
	public void getFormattedInstant_WithAfricaCairo_ShouldReturnFormattedDate() {
		String formattedDate = dateFormatter.getFormattedInstant(INSTANT_year2013_month12_day22_hour17_min42_sec8_milli33, TIMEZONE_ID_AFRICA_CAIRO, "yyyy MM dd HH:mm:ss");
		// Cairo is +2h so h17 is h19
		assertThat(formattedDate, is("2013 12 22 19:42:08"));
	}

	@Test
	public void getFormattedInstant_ShouldReturnStringWithLongPattern() {
		String expectedValue = "2013-12-22T17:42:08.033Z";
		Instant instant = Instant.ofEpochMilli(MILLIS_year2013_month12_day22_hour17_min42_sec8_milli33);
		assertThat(dateFormatter.getFormattedInstant(instant), is(expectedValue));
	}

	@Test
	public void getFormattedInstant_WithNullInstant_ShouldReturnEmpty() {
		Instant instant = null;
		assertThat(dateFormatter.getFormattedInstant(instant), isEmptyString());
	}

	@Test
	public void getInstantFromDate_WithNullDate_ShouldReturnNull() {
		Date value = null;
		assertThat(dateFormatter.getInstantFromDate(value), nullValue());
	}

	@Test
	public void getInstantFromDate_WithValidDate_ShouldReturnInstant() {
		Date value = DATE_year2013_month12_day22_hour17_min42_sec8_milli33;
		Instant expectedValue = Instant.ofEpochMilli(MILLIS_year2013_month12_day22_hour17_min42_sec8_milli33);
		assertThat(dateFormatter.getInstantFromDate(value), is(expectedValue));
	}

	@Test
	public void getInstantFromMillis_WithNullMillis_ShouldReturnNull() {
		Long value = null;
		assertThat(dateFormatter.getInstantFromMillis(value), nullValue());
	}

	@Test
	public void getDateFromMillis_WithNullMillis_ThenReturnsEmptyOptional() {
		Long value = null;
		assertThat(dateFormatter.getDateFromMillis(value), is(Optional.empty()));
	}

	@Test
	public void getDateFromMillis_WithValidMillis_ThenReturnsOptionalWithDate() {
		Long value = MILLIS_year2013_month12_day22_hour17_min42_sec8_milli33;
		Date expectedValue = new Date(MILLIS_year2013_month12_day22_hour17_min42_sec8_milli33);
		assertThat(dateFormatter.getDateFromMillis(value).get(), is(expectedValue));
	}

	@Test
	public void getInstantFromDate_WithValidMillis_ShouldReturnInstant() {
		Long value = MILLIS_year2013_month12_day22_hour17_min42_sec8_milli33;
		Instant expectedValue = Instant.ofEpochMilli(MILLIS_year2013_month12_day22_hour17_min42_sec8_milli33);
		assertThat(dateFormatter.getInstantFromMillis(value), is(expectedValue));
	}

	@Test
	public void getLongFormatDateFromCompactDate_WhenStringIsEmpty_ShouldReturnEmptyString() {
		assertThat(dateFormatter.getLongFormatDateFromCompactDate(""), isEmptyString());
	}

	@Test
	public void getLongFormatDateFromCompactDate_WhenStringNotNumber_ShouldReturnEmptyString() {
		assertThat(dateFormatter.getLongFormatDateFromCompactDate("abc"), isEmptyString());
	}

	@Test
	public void getLongFormatDateFromCompactDate_WhenStringIsValid_ShouldReturnFormattedString() {
		String value = "20121222081356";
		String expectedValue = "2012-12-22T08:13:56.000Z";
		assertThat(dateFormatter.getLongFormatDateFromCompactDate(value), is(expectedValue));
	}

	@Test
	public void getNow__ShouldReturnCurrentInstantInUTC() {
		Instant expectedValue = Instant.now(Clock.system(ZoneId.of("UTC")));
		String expectedValueString = dateFormatter.getFormattedInstant(expectedValue, TIMEZONE_ID_UTC, "yyyy MM dd HH:mm");

		Instant now = dateFormatter.getNow();
		String nowValueString = dateFormatter.getFormattedInstant(now, TIMEZONE_ID_UTC, "yyyy MM dd HH:mm");
		assertThat(nowValueString, is(expectedValueString));
	}

	@Test
	public void getInstantFromLongFormatString_WithEmptyValue_ReturnsNull() {
		Instant result = dateFormatter.getInstantFromLongFormatString("");
		assertThat(result, is(nullValue()));
	}

	@Test
	public void getInstantFromLongFormatString_WithInvalidValue_ReturnsNull() {
		Instant result = dateFormatter.getInstantFromLongFormatString("2016-14-16");
		assertThat(result, is(nullValue()));
	}

	@Test
	public void getInstantFromLongFormatString_WithValidValue_ReturnsInstant() {
		Instant expectedValue = Instant.ofEpochMilli(MILLIS_year2013_month12_day22_hour17_min42_sec8_milli33);

		Instant result = dateFormatter.getInstantFromLongFormatString("2013-12-22T17:42:08.033Z");

		assertThat(result, is(expectedValue));
	}

	@Test
	public void getFriendlyTime_WithInvalidInstant_ReturnsEmptyString() {
		String result = dateFormatter.getFriendlyTime(null, TIMEZONE_ID_EUROPE_LONDON, locale);
		assertThat(result, isEmptyString());
	}

	@Test
	public void getFriendlyTime_WithValidInstant_OverAYearAgo_ReturnsMonthAndYearValue() {
		LocalDateTime ldt = getNowForAfricaCairo();
		ldt = ldt.minusYears(3);
		Instant instant = getInstantFromLocalDateTime(ldt);

		String expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_AFRICA_CAIRO, DatePatterns.MONTH_YEAR);

		String result = dateFormatter.getFriendlyTime(instant, TIMEZONE_ID_AFRICA_CAIRO, locale);
		assertThat(result, is(expected));
	}

	@Test
	public void getFriendlyTime_WithValidInstant_DifferentYear_LessThan12MonthsDifference_ReturnsMonthAndYearValue() {
		LocalDateTime ldt = getNowForAfricaCairo();
		ldt = ldt.minusYears(1);
		ldt = ldt.minusMonths(2);
		Instant instant = getInstantFromLocalDateTime(ldt);

		String expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_AFRICA_CAIRO, DatePatterns.MONTH_YEAR);

		String result = dateFormatter.getFriendlyTime(instant, TIMEZONE_ID_AFRICA_CAIRO, locale);
		assertThat(result, is(expected));
	}

	@Test
	public void getFriendlyTime_WithValidInstant_SameYear_OverAMonthAgo_ReturnsMonthValue() {
		LocalDateTime ldt = getNowForAfricaCairo();
		int year = ldt.getYear();
		ldt = ldt.minusMonths(1);
		Instant instant = getInstantFromLocalDateTime(ldt);

		String expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_AFRICA_CAIRO, DatePatterns.MONTH);

		if (ldt.getYear() != year) {
			// Need to add this in case the minus operation changes the year
			expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_AFRICA_CAIRO, DatePatterns.MONTH_YEAR);
		}

		String result = dateFormatter.getFriendlyTime(instant, TIMEZONE_ID_AFRICA_CAIRO, locale);
		assertThat(result, is(expected));
	}

	@Test
	public void getFriendlyTime_WithValidInstant_SameYear_SameMonth_OverAweekAgo_ReturnsThisMonth() {
		LocalDateTime ldt = getNowForAfricaCairo();
		int year = ldt.getYear();
		int month = ldt.getMonthValue();
		ldt = ldt.minusDays(8);
		Instant instant = getInstantFromLocalDateTime(ldt);

		String expected = localeMapUtil.getResourceBundle(locale).getString("time-this-month");
		if (ldt.getYear() != year) {
			// Need to add this in case the minus operation changes the year
			expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_AFRICA_CAIRO, DatePatterns.MONTH_YEAR);
		} else if (ldt.getMonthValue() != month) {
			// Need to add this in case the minus operation changes the month
			expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_AFRICA_CAIRO, DatePatterns.MONTH);
		}

		String result = dateFormatter.getFriendlyTime(instant, TIMEZONE_ID_AFRICA_CAIRO, locale);
		assertThat(result, is(expected));
	}

	@Test
	public void getFriendlyTime_WithValidInstant_SameYear_SameMonth_MoreThanTwoDaysAgo_LessThenAweekAgo_ReturnsThisWeek() {
		LocalDateTime ldt = getNowForAfricaCairo();
		int year = ldt.getYear();
		int month = ldt.getMonthValue();
		ldt = ldt.minusDays(2);
		Instant instant = getInstantFromLocalDateTime(ldt);

		String expected = localeMapUtil.getResourceBundle(locale).getString("time-this-week");
		if (ldt.getYear() != year) {
			// Need to add this in case the minus operation changes the year
			expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_EUROPE_LONDON, DatePatterns.MONTH_YEAR);
		} else if (ldt.getMonthValue() != month) {
			// Need to add this in case the minus operation changes the month
			expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_EUROPE_LONDON, DatePatterns.MONTH);
		}

		String result = dateFormatter.getFriendlyTime(instant, TIMEZONE_ID_EUROPE_LONDON, locale);
		assertThat(result, is(expected));
	}

	@Test
	public void getFriendlyTime_WithValidInstant_SameYear_SameMonth_OneDayAgo_ReturnsYesterday() {
		LocalDateTime ldt = getNowForAfricaCairo();
		int year = ldt.getYear();
		int month = ldt.getMonthValue();
		ldt = ldt.minusDays(1);
		Instant instant = getInstantFromLocalDateTime(ldt);

		String expected = localeMapUtil.getResourceBundle(locale).getString("time-yesterday");
		if (ldt.getYear() != year) {
			// Need to add this in case the minus operation changes the year
			expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_EUROPE_LONDON, DatePatterns.MONTH_YEAR);
		} else if (ldt.getMonthValue() != month) {
			// Need to add this in case the minus operation changes the month
			expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_EUROPE_LONDON, DatePatterns.MONTH);
		}

		String result = dateFormatter.getFriendlyTime(instant, TIMEZONE_ID_EUROPE_LONDON, locale);
		assertThat(result, is(expected));
	}

	@Test
	public void getFriendlyTime_WithValidInstant_OfToday_ReturnsToday() {
		LocalDateTime ldt = getNowForAfricaCairo();
		Instant instant = getInstantFromLocalDateTime(ldt);
		String expected = localeMapUtil.getResourceBundle(locale).getString("time-today");
		String result = dateFormatter.getFriendlyTime(instant, TIMEZONE_ID_EUROPE_LONDON, locale);
		assertThat(result, is(expected));
	}

	@Test
	public void getFriendlyDate_WithInvalidInstant_ReturnsEmptyString() {
		String result = dateFormatter.getFriendlyDate(null, TIMEZONE_ID_EUROPE_LONDON);
		assertThat(result, isEmptyString());
	}

	@Test
	public void getFriendlyDate_WithValidInstant_DifferentYear_ReturnsDayAndMonthAndYearValue() {
		LocalDateTime ldt = getNowForAfricaCairo();
		ldt = ldt.minusYears(2);
		Instant instant = getInstantFromLocalDateTime(ldt);

		String expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_AFRICA_CAIRO, DatePatterns.DAY_MONTH_YEAR);

		String result = dateFormatter.getFriendlyDate(instant, TIMEZONE_ID_AFRICA_CAIRO);
		assertThat(result, is(expected));
	}

	@Test
	public void getFriendlyDate_WithValidInstant_SameYear_ReturnsDayAndMonthValue() {
		LocalDateTime ldt = getNowForAfricaCairo();
		Instant instant = getInstantFromLocalDateTime(ldt);

		String expected = dateFormatter.getFormattedInstant(instant, TIMEZONE_ID_AFRICA_CAIRO, DatePatterns.DAY_MONTH);

		String result = dateFormatter.getFriendlyDate(instant, TIMEZONE_ID_AFRICA_CAIRO);
		assertThat(result, is(expected));
	}

	private Instant getInstantFromLocalDateTime(LocalDateTime ldt) {
		return ldt.toInstant(ZoneOffset.UTC);
	}

	private LocalDateTime getNowForAfricaCairo() {
		return LocalDateTime.ofInstant(Instant.now(), ZONE_ID_AFRICA_CAIRO);
	}
}
