/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.time;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.pfiks.common.time.model.DateTimeSelection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import javax.portlet.PortletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DateTimeSelectionServiceTest {

	private DateTimeSelectionService dateTimeSelectionService;

	private static final int AM = Calendar.AM;
	private static final int HOUR = 3;
	private static final int MINUTE = 45;
	private static final int YEAR = 2000;
	private static final int MONTH = 7;
	private static final int DAY = 15;
	private static final String FIELD_PREFIX = "fieldPrefix";

	private TimeZone timeZone = TimeZone.getTimeZone("Africa/Cairo");

	@Mock
	private PortletRequest mockPortletRequest;

	@Mock
	private Date mockDate;

	@Mock
	private DateTimeSelection mockDateTimeSelection;

	private MockedStatic<ParamUtil> mockParamUtil;

	private MockedStatic<PortalUtil> mockPortalUtil;

	private MockedStatic<PropsUtil> mockPropsUtil;

	@Mock
	private TimeZone mockTimeZone;

	@Before
	public void setUp() {

		mockPropsUtil = mockStatic(PropsUtil.class);
		mockParamUtil = mockStatic(ParamUtil.class);
		mockPortalUtil = mockStatic(PortalUtil.class);

		dateTimeSelectionService = new DateTimeSelectionService();

	}

	@After
	public void teardown() {

		mockPropsUtil.close();
		mockParamUtil.close();
		mockPortalUtil.close();

	}


	@Test
	public void getDateTimeSelectionFromDate_WhenNoError_ThenReturnsADateTimeSelectionWithValuesFromTheDateInTheSpecifiedTimeZone() {
		Calendar expected = Calendar.getInstance(timeZone);
		expected.add(Calendar.HOUR, 3);
		Date date = expected.getTime();

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionFromDate(date, timeZone);

		verifyResultMatchesCalendar(result, expected);
	}

	@Test
	public void getDateTimeSelection_WhenNoError_ThenReturnsADateTimeSelectionWithValuesFromNowInTheSpecifiedTimeZone() {
		Calendar expected = Calendar.getInstance(timeZone);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelection(timeZone);

		verifyResultMatchesCalendar(result, expected);
	}

	@Test
	public void getDateTimeSelectionWithHours_WithValidHourToAddValue_ThenReturnsADateTimeSelectionWithValuesFromNowPlusTheHourSpecifiedInTheSpecifiedTimeZone() {
		int hoursToAdd = 5;
		Calendar expected = Calendar.getInstance(timeZone);
		expected.add(Calendar.HOUR, hoursToAdd);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionWithHours(timeZone, hoursToAdd);

		verifyResultMatchesCalendar(result, expected);
	}

	@Test
	public void getDateTimeSelectionWithHours_WithInvalidHourToAddValue_ThenReturnsADateTimeSelectionWithValuesFromNowInTheSpecifiedTimeZone() {
		int hoursToAdd = 0;
		Calendar expected = Calendar.getInstance(timeZone);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionWithHours(timeZone, hoursToAdd);

		verifyResultMatchesCalendar(result, expected);
	}

	@Test
	public void getDateTimeSelectionWithMonths_WithValidMonthToAddValue_ThenReturnsADateTimeSelectionWithValuesFromNowPlusTheHourSpecifiedInTheSpecifiedTimeZone() {
		int montsToAdd = 2;
		Calendar expected = Calendar.getInstance(timeZone);
		expected.add(Calendar.MONTH, montsToAdd);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionWithMonths(timeZone, montsToAdd);

		verifyResultMatchesCalendar(result, expected);
	}

	@Test
	public void getDateTimeSelectionWithMonths_WithInvalidMonthToAddValue_ThenReturnsADateTimeSelectionWithValuesFromNowInTheSpecifiedTimeZone() {
		int monthsToAdd = 0;
		Calendar expected = Calendar.getInstance(timeZone);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionWithMonths(timeZone, monthsToAdd);

		verifyResultMatchesCalendar(result, expected);
	}

	@Test
	public void getDateFromDateTimeSelection_WithDateTimeSelectionAndTimeZoneParams_WhenDateTimeSelectionIsValid_ThenReturnsTheDateWithDateTimeSelectionDateAndTimeValues() throws Exception {

		mockDateTimeSelectionValues(MONTH, DAY, YEAR, HOUR, MINUTE);
		mockPortalUtil.when(() -> PortalUtil.getDate(MONTH, DAY, YEAR, HOUR, MINUTE, mockTimeZone, PortalException.class)).thenReturn(mockDate);

		Date result = dateTimeSelectionService.getDateFromDateTimeSelection(mockDateTimeSelection, mockTimeZone);

		assertThat(result, sameInstance(mockDate));

	}

	@Test(expected = PortalException.class)
	public void getDateFromDateTimeSelection_WithDateTimeSelectionAndTimeZoneParams_WhenDateTimeSelectionIsInvalid_ThenThrowsPortalException() throws Exception {

		mockDateTimeSelectionValues(MONTH, DAY, YEAR, HOUR, MINUTE);
		mockPortalUtil.when(() -> PortalUtil.getDate(MONTH, DAY, YEAR, HOUR, MINUTE, mockTimeZone, PortalException.class)).thenThrow(new PortalException());

		dateTimeSelectionService.getDateFromDateTimeSelection(mockDateTimeSelection, mockTimeZone);

	}

	@Test
	public void getDateTimeSelectionFromMillis_WhenNoError_ThenReturnsADateTimeSelectionObjectConfiguredWithValuesFromTheMillisInTheSpecifiedTimeZone() {
		Calendar calendar = Calendar.getInstance(timeZone);
		calendar.add(Calendar.MONTH, 3);
		long millis = calendar.getTimeInMillis();

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionFromMillis(millis, timeZone);

		verifyResultMatchesCalendar(result, calendar);
	}

	@Test
	public void getDateFromDateTimeSelection_WithIncludeTimeAndDateTimeSelectionAndTimeZoneParams_WhenDateTimeSelectionIsValidAndIncludeTimeIsFalse_ThenReturnsOptionalWithTheDateWithDateTimeSelectionDateValues() {

		mockDateTimeSelectionValues(MONTH, DAY, YEAR);
		mockPortalUtil.when(() -> PortalUtil.getDate(MONTH, DAY, YEAR, mockTimeZone, PortalException.class)).thenReturn(mockDate);

		Optional<Date> result = dateTimeSelectionService.getDateFromDateTimeSelection(false, mockDateTimeSelection, mockTimeZone);

		assertThat(result.get(), sameInstance(mockDate));


	}

	@Test
	public void getDateFromDateTimeSelection_WithIncludeTimeAndDateTimeSelectionAndTimeZoneParams_WhenDateTimeSelectionIsValidAndIncludeTimeIsTrue_ThenReturnsOptionalWithTheDateWithDateTimeSelectionDateValues() {

		mockDateTimeSelectionValues(MONTH, DAY, YEAR, HOUR, MINUTE);
		mockPortalUtil.when(() -> PortalUtil.getDate(MONTH, DAY, YEAR, HOUR, MINUTE, mockTimeZone, PortalException.class)).thenReturn(mockDate);

		Optional<Date> result = dateTimeSelectionService.getDateFromDateTimeSelection(true, mockDateTimeSelection, mockTimeZone);

		assertThat(result.get(), sameInstance(mockDate));

	}

	@Test
	public void getDateFromDateTimeSelection_WithIncludeTimeAndDateTimeSelectionAndTimeZoneParams_WhenDateTimeSelectionIsInvalidAndIncludeTimeIsTrue_ThenReturnsEmptyOptional() throws Exception {

		mockDateTimeSelectionValues(MONTH, DAY, YEAR, HOUR, MINUTE);
		mockPortalUtil.when(() -> PortalUtil.getDate(MONTH, DAY, YEAR, HOUR, MINUTE, mockTimeZone, PortalException.class)).thenThrow(new PortalException());

		Optional<Date> result = dateTimeSelectionService.getDateFromDateTimeSelection(true, mockDateTimeSelection, mockTimeZone);

		assertThat(result.isPresent(), equalTo(false));

	}

	@Test
	public void getDateFromDateTimeSelection_WithIncludeTimeAndDateTimeSelectionAndTimeZoneParams_WhenDateTimeSelectionIsInvalidAndIncludeTimeIsFalse_ThenReturnsEmptyOptional() throws Exception {

		mockDateTimeSelectionValues(MONTH, DAY, YEAR);
		mockPortalUtil.when(() -> PortalUtil.getDate(MONTH, DAY, YEAR, mockTimeZone, PortalException.class)).thenThrow(new PortalException());

		Optional<Date> result = dateTimeSelectionService.getDateFromDateTimeSelection(false, mockDateTimeSelection, mockTimeZone);

		assertThat(result.isPresent(), equalTo(false));

	}

	@Test
	public void getDateTimeSelectionFromRequest_WhenAM_ThenReturnsDateTimeSelectionWithHourFromRequest() {
		mockRequestValues(true);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionFromRequest(mockPortletRequest, FIELD_PREFIX);

		assertThat(result.getHour(), equalTo(HOUR));
	}

	@Test
	public void getDateTimeSelectionFromRequest_WhenAPM_ThenReturnsDateTimeSelectionWithHourFromRequestPlus12() {
		mockRequestValues(false);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionFromRequest(mockPortletRequest, FIELD_PREFIX);

		assertThat(result.getHour(), equalTo(HOUR + 12));
	}

	@Test
	public void getDateTimeSelectionFromRequest_WhenAM_ThenReturnsDateTimeSelectionWithAMPMFromRequest() {
		mockRequestValues(true);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionFromRequest(mockPortletRequest, FIELD_PREFIX);

		assertThat(result.getAmORpm(), equalTo(AM));
	}

	@Test
	public void getDateTimeSelectionFromRequest_WhenAM_ThenReturnsDateTimeSelectionWithYearFromRequest() {
		mockRequestValues(true);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionFromRequest(mockPortletRequest, FIELD_PREFIX);

		assertThat(result.getYear(), equalTo(YEAR));
	}

	@Test
	public void getDateTimeSelectionFromRequest_WhenAM_ThenReturnsDateTimeSelectionWithMonthFromRequest() {
		mockRequestValues(true);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionFromRequest(mockPortletRequest, FIELD_PREFIX);

		assertThat(result.getMonth(), equalTo(MONTH));
	}

	@Test
	public void getDateTimeSelectionFromRequest_WhenAM_ThenReturnsDateTimeSelectionWithDayFromRequest() {
		mockRequestValues(true);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionFromRequest(mockPortletRequest, FIELD_PREFIX);

		assertThat(result.getDay(), equalTo(DAY));
	}

	@Test
	public void getDateTimeSelectionFromRequest_WhenAM_ThenReturnsDateTimeSelectionWithMinuteFromRequest() {
		mockRequestValues(true);

		DateTimeSelection result = dateTimeSelectionService.getDateTimeSelectionFromRequest(mockPortletRequest, FIELD_PREFIX);

		assertThat(result.getMinute(), equalTo(MINUTE));
	}

	private void mockRequestValues(boolean isAM) {

		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, FIELD_PREFIX + "Hour", 0)).thenReturn(HOUR);

		if (isAM) {
			mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, FIELD_PREFIX + "AMorPM", Calendar.AM)).thenReturn(Calendar.AM);
		} else {
			mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, FIELD_PREFIX + "AMorPM", Calendar.AM)).thenReturn(Calendar.PM);
		}

		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, FIELD_PREFIX + "Year")).thenReturn(YEAR);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, FIELD_PREFIX + "Month")).thenReturn(MONTH);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, FIELD_PREFIX + "Day")).thenReturn(DAY);
		mockParamUtil.when(() -> ParamUtil.getInteger(mockPortletRequest, FIELD_PREFIX + "Minute", 0)).thenReturn(MINUTE);

	}

	private void verifyResultMatchesCalendar(DateTimeSelection result, Calendar calendarToMatch) {
		assertThat(result.getYear(), equalTo(calendarToMatch.get(Calendar.YEAR)));
		assertThat(result.getMonth(), equalTo(calendarToMatch.get(Calendar.MONTH)));
		assertThat(result.getDay(), equalTo(calendarToMatch.get(Calendar.DAY_OF_MONTH)));
		assertThat(result.getHour(), equalTo(calendarToMatch.get(Calendar.HOUR)));
		assertThat(result.getMinute(), equalTo(calendarToMatch.get(Calendar.MINUTE)));
		assertThat(result.getAmORpm(), equalTo(calendarToMatch.get(Calendar.AM_PM)));
	}

	private void mockDateTimeSelectionValues(int month, int day, int year) {
		mockDateTimeSelectionValues(month, day, year, 0, 0);
	}

	private void mockDateTimeSelectionValues(int month, int day, int year, int hour, int minute) {
		when(mockDateTimeSelection.getHour()).thenReturn(hour);
		when(mockDateTimeSelection.getMinute()).thenReturn(minute);
		when(mockDateTimeSelection.getYear()).thenReturn(year);
		when(mockDateTimeSelection.getMonth()).thenReturn(month);
		when(mockDateTimeSelection.getDay()).thenReturn(day);
	}

}
