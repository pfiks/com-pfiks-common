/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.time.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Test;

public class DateTimeSelectionTest {

	private static final int DAY = 5;
	private static final int MONTH = 6;
	private static final int YEAR = 2010;
	private static final int HOUR = 3;
	private static final int MINUTE = 7;
	private static final int AM_PM = Calendar.AM;

	@Test
	public void newDateTimeSelection_ConfiguresTheValuesBasedOnTheCalendar() {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, 1);
		calendar.add(Calendar.MONTH, 3);
		calendar.add(Calendar.HOUR, 27);
		calendar.add(Calendar.MINUTE, 15);

		DateTimeSelection dateTimeSelection = new DateTimeSelection(calendar);

		assertThat(dateTimeSelection.getYear(), equalTo(calendar.get(Calendar.YEAR)));
		assertThat(dateTimeSelection.getMonth(), equalTo(calendar.get(Calendar.MONTH)));
		assertThat(dateTimeSelection.getDay(), equalTo(calendar.get(Calendar.DAY_OF_MONTH)));
		assertThat(dateTimeSelection.getAmORpm(), equalTo(calendar.get(Calendar.AM_PM)));
		assertThat(dateTimeSelection.getHour(), equalTo(calendar.get(Calendar.HOUR)));
		assertThat(dateTimeSelection.getMinute(), equalTo(calendar.get(Calendar.MINUTE)));
	}

	@Test
	public void getDate_WhenNoError_ThenReturnsADateWithTheYearConfiguredAsTheDateTimeSelectionValue() {
		TimeZone timeZone = TimeZone.getDefault();

		DateTimeSelection dateTimeSelection = createDateTimeSelectionWithValues();

		Date result = dateTimeSelection.getDate(timeZone);
		Calendar calendar = Calendar.getInstance(timeZone);
		calendar.setTime(result);
		assertThat(calendar.get(Calendar.YEAR), equalTo(YEAR));
	}

	@Test
	public void getDate_WhenNoError_ThenReturnsADateWithTheMonthConfiguredAsTheDateTimeSelectionValue() {
		TimeZone timeZone = TimeZone.getDefault();

		DateTimeSelection dateTimeSelection = createDateTimeSelectionWithValues();

		Date result = dateTimeSelection.getDate(timeZone);
		Calendar calendar = Calendar.getInstance(timeZone);
		calendar.setTime(result);
		assertThat(calendar.get(Calendar.MONTH), equalTo(MONTH));
	}

	@Test
	public void getDate_WhenNoError_ThenReturnsADateWithTheDayOfMonthConfiguredAsTheDateTimeSelectionValue() {
		TimeZone timeZone = TimeZone.getDefault();

		DateTimeSelection dateTimeSelection = createDateTimeSelectionWithValues();

		Date result = dateTimeSelection.getDate(timeZone);
		Calendar calendar = Calendar.getInstance(timeZone);
		calendar.setTime(result);
		assertThat(calendar.get(Calendar.DAY_OF_MONTH), equalTo(DAY));
	}

	@Test
	public void getDate_WhenNoError_ThenReturnsADateWithTheAM_PMConfiguredAsTheDateTimeSelectionValue() {
		TimeZone timeZone = TimeZone.getDefault();

		DateTimeSelection dateTimeSelection = createDateTimeSelectionWithValues();

		Date date = dateTimeSelection.getDate(timeZone);
		Calendar calendar = Calendar.getInstance(timeZone);
		calendar.setTime(date);
		assertThat(calendar.get(Calendar.AM_PM), equalTo(AM_PM));
	}

	@Test
	public void getDate_WhenNoError_ThenReturnsADateWithTheHourConfiguredAsTheDateTimeSelectionValue() {
		TimeZone timeZone = TimeZone.getDefault();

		DateTimeSelection dateTimeSelection = createDateTimeSelectionWithValues();

		Date date = dateTimeSelection.getDate(timeZone);
		Calendar calendar = Calendar.getInstance(timeZone);
		calendar.setTime(date);
		assertThat(calendar.get(Calendar.HOUR), equalTo(HOUR));
	}

	@Test
	public void getDate_WhenNoError_ThenReturnsADateWithTheMinuteConfiguredAsTheDateTimeSelectionValue() {
		TimeZone timeZone = TimeZone.getDefault();

		DateTimeSelection dateTimeSelection = createDateTimeSelectionWithValues();

		Date date = dateTimeSelection.getDate(timeZone);
		Calendar calendar = Calendar.getInstance(timeZone);
		calendar.setTime(date);
		assertThat(calendar.get(Calendar.MINUTE), equalTo(MINUTE));
	}

	private DateTimeSelection createDateTimeSelectionWithValues() {
		DateTimeSelection dateTimeSelection = new DateTimeSelection();
		dateTimeSelection.setDay(DAY);
		dateTimeSelection.setMonth(MONTH);
		dateTimeSelection.setYear(YEAR);
		dateTimeSelection.setHour(HOUR);
		dateTimeSelection.setMinute(MINUTE);
		dateTimeSelection.setAmORpm(AM_PM);
		return dateTimeSelection;
	}

}
