/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.portlet;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Layout;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletURL;
import javax.portlet.RenderResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.sameInstance;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PortletURLServiceImplTest {

	private final static String FULL_REDIRECT_URL = "/full/redirect/url?with=parameters";

	private final static String PAGE_REDIRECT_URL = "/page/redirect/url";

	@Mock
	private MutableRenderParameters mockMutableRenderParameters;

	private MockedStatic<PortalUtil> mockPortalUtil;

	@Mock
	private PortletURL mockPortletURL;

	@Mock
	private RenderResponse mockRenderResponse;

	@Mock
	private ThemeDisplay mockThemeDisplay;

	@Mock
	private Layout mockLayout;

	private PortletURLServiceImpl portletURLServiceImpl;

	@Before
	public void setUp() throws Exception {

		mockPortalUtil = mockStatic(PortalUtil.class);

		when(mockPortletURL.getRenderParameters()).thenReturn(mockMutableRenderParameters);
		when(mockRenderResponse.createRenderURL()).thenReturn(mockPortletURL);
		when(mockThemeDisplay.getURLCurrent()).thenReturn(FULL_REDIRECT_URL);
		when(mockThemeDisplay.getLayout()).thenReturn(mockLayout);

		portletURLServiceImpl = new PortletURLServiceImpl();

	}

	@After
	public void teardown() {
		mockPortalUtil.close();
	}

	@Test
	public void getPortletURL_WithNullParameters_ReturnsPortletURLWithoutParameters() {

		PortletURL portletURL = portletURLServiceImpl.getPortletURL(mockRenderResponse, null);

		assertThat(portletURL, sameInstance(mockPortletURL));

		verify(mockMutableRenderParameters, never()).setValue(anyString(), anyString());
	}

	@Test
	public void getPortletURL_WithParameters_ReturnsPortletURLWithParameters() {
		String param1Key = "param1Key";
		String param1Value = "param1Value";
		String param2Key = "param2Key";
		String param2Value = "param1Value";

		Map<String, String> parameters = new HashMap<>();
		parameters.put(param1Key, param1Value);
		parameters.put(param2Key, param2Value);

		PortletURL portletURL = portletURLServiceImpl.getPortletURL(mockRenderResponse, parameters);

		assertThat(portletURL, sameInstance(mockPortletURL));
		verify(mockMutableRenderParameters, times(1)).setValue(param1Key, param1Value);
		verify(mockMutableRenderParameters, times(1)).setValue(param2Key, param2Value);
	}

	@Test
	public void getRedirectURL_WhenLifecycleIsRenderPhase_ReturnsFullUrl() {
		when(mockThemeDisplay.getLifecycle()).thenReturn("0");
		Optional<String> redirect = portletURLServiceImpl.getRedirectURL(mockThemeDisplay, false);
		assertThat(redirect.get(), equalTo(FULL_REDIRECT_URL));
	}

	@Test
	public void getRedirectURL_WhenLifecycleIsActionPhase_ReturnsFullUrl() {
		when(mockThemeDisplay.getLifecycle()).thenReturn("1");
		Optional<String> redirect = portletURLServiceImpl.getRedirectURL(mockThemeDisplay, false);
		assertThat(redirect.get(), equalTo(FULL_REDIRECT_URL));
	}

	@Test
	public void getRedirectURL_WhenLifecycleIsResourcePhase_ReturnsPageUrl() {

		when(mockThemeDisplay.getLifecycle()).thenReturn("2");
		mockPortalUtil.when(() -> PortalUtil.getLayoutRelativeURL(mockLayout, mockThemeDisplay)).thenReturn(PAGE_REDIRECT_URL);

		Optional<String> redirect = portletURLServiceImpl.getRedirectURL(mockThemeDisplay, false);

		assertThat(redirect.get(), equalTo(PAGE_REDIRECT_URL));

	}

	@Test
	public void getRedirectURL_WhenEncodingRequested_ReturnsEncodedUrl() throws UnsupportedEncodingException {
		when(mockThemeDisplay.getLifecycle()).thenReturn("0");
		Optional<String> redirect = portletURLServiceImpl.getRedirectURL(mockThemeDisplay, true);
		assertThat(redirect.get(), equalTo(URLEncoder.encode(FULL_REDIRECT_URL, "UTF-8")));
	}

	@Test
	public void getRedirectURL_WhenExceptionRetrievingLayoutRelativeUrl_ReturnsEmptyOptional() throws Exception {

		when(mockThemeDisplay.getLifecycle()).thenReturn("2");
		mockPortalUtil.when(() -> PortalUtil.getLayoutRelativeURL(mockLayout, mockThemeDisplay)).thenThrow(new PortalException());

		Optional<String> redirect = portletURLServiceImpl.getRedirectURL(mockThemeDisplay, false);

		assertThat(redirect.isPresent(), equalTo(false));

	}

}
