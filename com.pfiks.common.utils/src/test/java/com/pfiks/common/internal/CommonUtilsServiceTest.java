/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.internal;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.UnicodeProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

@RunWith(MockitoJUnitRunner.class)
public class CommonUtilsServiceTest {

	private CommonUtilsService commonUtilsService;

	@Mock
	private Group mockGroup;

	@Mock
	private UnicodeProperties mockUnicodeProperties;

	@Before
	public void setUp() {
		commonUtilsService = new CommonUtilsService();
	}

	@Test
	public void getAvailableLocales_WhenTypeSettingsPropertiesIsNull_ThenReturnsTheGroupLocales() {
		String[] groupLocales = new String[]{"one", "two", "three"};
		when(mockUnicodeProperties.getProperty(PropsKeys.LOCALES)).thenReturn(StringPool.SPACE);
		when(mockGroup.getAvailableLanguageIds()).thenReturn(groupLocales);

		String result = commonUtilsService.getAvailableLocales(mockGroup, mockUnicodeProperties);

		assertThat(result, equalTo(String.join(",", groupLocales)));
	}

	@Test
	public void getAvailableLocales_WhenTypeSettingsPropertiesIsValid_ThenReturnsTheTypeSettingLocales() {
		String expected = "expectedValue";
		when(mockUnicodeProperties.getProperty(PropsKeys.LOCALES)).thenReturn(expected);

		String result = commonUtilsService.getAvailableLocales(mockGroup, mockUnicodeProperties);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getLanguageId_WhenTypeSettingsPropertiesIsNull_ThenReturnsTheGroupLanguageId() {
		String expected = "expectedValue";
		when(mockUnicodeProperties.getProperty("languageId")).thenReturn(StringPool.SPACE);
		when(mockGroup.getDefaultLanguageId()).thenReturn(expected);

		String result = commonUtilsService.getLanguageId(mockGroup, mockUnicodeProperties);

		assertThat(result, equalTo(expected));
	}

	@Test
	public void getLanguageId_WhenTypeSettingsPropertiesIsValid_ThenReturnsTheTypeSettingLanguageId() {
		String expected = "expectedValue";
		when(mockUnicodeProperties.getProperty("languageId")).thenReturn(expected);

		String result = commonUtilsService.getLanguageId(mockGroup, mockUnicodeProperties);

		assertThat(result, equalTo(expected));
	}
}
