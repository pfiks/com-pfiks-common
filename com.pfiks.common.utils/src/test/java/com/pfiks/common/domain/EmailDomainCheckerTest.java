/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.domain;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.assertThat;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.liferay.petra.string.StringPool;

public class EmailDomainCheckerTest {

	private EmailDomainChecker emailDomainChecker;

	@Before
	public void setUp() {
		emailDomainChecker = new EmailDomainChecker();
	}

	@Test
	public void getDomainPartsFromEmail_WhenEmailIsNull_ThenReturnsEmptySet() {
		String emailAddress = null;

		Set<String> results = emailDomainChecker.getDomainPartsFromEmail(emailAddress);

		assertThat(results, empty());
	}

	@Test
	public void getDomainPartsFromEmail_WhenEmailIsBlank_ThenReturnsEmptySet() {
		String emailAddress = StringPool.THREE_SPACES;

		Set<String> results = emailDomainChecker.getDomainPartsFromEmail(emailAddress);

		assertThat(results, empty());
	}

	@Test
	public void getDomainPartsFromEmail_WhenEmailIsValid_ThenReturnsAllTheDomainPartsForTheEmailAddress() {
		String emailAddress = "test@one.two.three.four";

		Set<String> results = emailDomainChecker.getDomainPartsFromEmail(emailAddress);

		assertThat(results, containsInAnyOrder("one.two.three.four", "two.three.four", "three.four", "four"));
	}

	@Test
	public void isEmailValidForDomain_WhenEmailIsNull_ThenReturnsFalse() {
		String emailAddress = null;
		String domain = "@domain.test";

		boolean result = emailDomainChecker.isEmailValidForDomain(emailAddress, domain);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomain_WhenEmailIsEmpty_ThenReturnsFalse() {
		String emailAddress = StringPool.THREE_SPACES;
		String domain = "@domain.test";

		boolean result = emailDomainChecker.isEmailValidForDomain(emailAddress, domain);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomain_WhenDomainIsNull_ThenReturnsFalse() {
		String emailAddress = "email@domain.test.one";
		String domain = null;

		boolean result = emailDomainChecker.isEmailValidForDomain(emailAddress, domain);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomain_WhenDomainIsEmpty_ThenReturnsFalse() {
		String emailAddress = "email@domain.test.one";
		String domain = StringPool.THREE_SPACES;

		boolean result = emailDomainChecker.isEmailValidForDomain(emailAddress, domain);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomain_WhenDomainStartsWithAtAndEmailDoesNotMatchInFull_ThenReturnsFalse() {
		String domain = " @domain.test";
		String emailAddress = "email@domain.test.one";

		boolean result = emailDomainChecker.isEmailValidForDomain(emailAddress, domain);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomain_WhenDomainStartsWithAtAndEmailDoesMatchInFull_ThenReturnsTrue() {
		String domain = " @domain.test";
		String emailAddress = "email@domain.test";

		boolean result = emailDomainChecker.isEmailValidForDomain(emailAddress, domain);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isEmailValidForDomain_WhenDomainDoesNotStartWithAtAndNoEmailDomainPartMatch_ThenReturnsFalse() {
		assertThat(emailDomainChecker.isEmailValidForDomain("email@one.two.domain.test.one", " domain.test"), equalTo(false));
		assertThat(emailDomainChecker.isEmailValidForDomain("email@one.two.domain.tes", " domain.test"), equalTo(false));
		assertThat(emailDomainChecker.isEmailValidForDomain("email@one.two.domaintest", " domain.test"), equalTo(false));
		assertThat(emailDomainChecker.isEmailValidForDomain("email@one.twodomain.test", " domain.test"), equalTo(false));
	}

	@Test
	public void isEmailValidForDomain_WhenDomainDoesNotStartWithAtAndAnEmailDomainPartMatches_ThenReturnsTrue() {
		String domain = " domain.test";
		String emailAddress = "email@one.two.domain.test";

		boolean result = emailDomainChecker.isEmailValidForDomain(emailAddress, domain);

		assertThat(result, equalTo(true));
	}

	@Test
	public void isEmailValidForDomains_WhenEmailIsNull_ThenReturnsFalse() {
		String emailAddress = null;
		String[] domains = new String[]{" @one.two", " two", " @one.two.domain", " one.two.domain"};

		boolean result = emailDomainChecker.isEmailValidForDomains(emailAddress, domains);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomains_WhenEmailIsEmpty_ThenReturnsFalse() {
		String emailAddress = StringPool.THREE_SPACES;
		String[] domains = new String[]{"@one.two", "two", "@one.two.domain", "one.two.domain"};

		boolean result = emailDomainChecker.isEmailValidForDomains(emailAddress, domains);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomains_WhenDomainsAreNull_ThenReturnsFalse() {
		String emailAddress = "email@one.two.domain.test";
		String[] domains = null;

		boolean result = emailDomainChecker.isEmailValidForDomains(emailAddress, domains);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomains_WhenDomainsAreEmpty_ThenReturnsFalse() {
		String emailAddress = "email@one.two.domain.test";
		String[] domains = new String[0];

		boolean result = emailDomainChecker.isEmailValidForDomains(emailAddress, domains);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomains_WhenEmailDoesNotMatchAnyOfTheDomains_ThenReturnsFalse() {
		String emailAddress = "email@one.two.domain.test";
		String[] domains = new String[]{"@one.two", "two", "@one.two.domain", "one.two.domain"};

		boolean result = emailDomainChecker.isEmailValidForDomains(emailAddress, domains);

		assertThat(result, equalTo(false));
	}

	@Test
	public void isEmailValidForDomains_WhenEmailDoesMatchesAnyOfTheDomains_ThenReturnsTrue() {
		assertThat(emailDomainChecker.isEmailValidForDomains("email@one.two.domain.test", new String[]{"@one.two", "two", "@one.two.domain", "@one.two.domain.test"}), equalTo(true));
		assertThat(emailDomainChecker.isEmailValidForDomains("email@one.two.domain.test", new String[]{"@one.two", "two", "@one.two.domain", "test"}), equalTo(true));
		assertThat(emailDomainChecker.isEmailValidForDomains("email@one.two.domain.test", new String[]{"@one.two", "two", "@one.two.domain", "domain.test"}), equalTo(true));
	}

}
