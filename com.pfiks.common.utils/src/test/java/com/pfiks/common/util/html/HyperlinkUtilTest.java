/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.util.html;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class HyperlinkUtilTest {

	private static final String TEST_TEXT = "test http://test.com test [https://www.test.co.uk|Test Test] test";

	@Test
	public void convertToHyperlink_WhenTextIsNull_ThenReturnsNull() {

		String result = HyperlinkUtil.convertToHyperlink(null);

		assertThat(result, nullValue());
	}

	@Test
	public void convertToHyperlink_WhenThereIsNotLink_ThenReturnsSameText() {

		String text = "test test";

		String result = HyperlinkUtil.convertToHyperlink(text);

		assertThat(result, equalTo(text));
	}

	@Test
	public void convertToHyperlink_WhenThereIsMalformedLink_ThenReturnsSameText() {

		String text = "test www.test.com test";

		String result = HyperlinkUtil.convertToHyperlink(text);

		assertThat(result, equalTo(text));
	}

	@Test
	public void convertToHyperlink_WhenThereIsWellformedLink_ThenConvertsText() {

		String expected = "test <a target=\"_blank\" href=\"http://test.com\">http://test.com</a> test [https://www.test.co.uk|Test Test] test";

		String result = HyperlinkUtil.convertToHyperlink(TEST_TEXT);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void decorateHyperlinks_WhenIsCalled_ThenDecorateLinks() {

		String expected = "test <a target=\"_blank\" href=\"http://test.com\">http://test.com</a> test <a target=\"_blank\" href=\"https://www.test.co.uk\">Test Test</a> test";

		String result = HyperlinkUtil.decorateHyperlinks(TEST_TEXT);

		assertThat(result, equalTo(expected));

	}

	@Test
	public void renderHyperlinkMarkup_WhenTextIsNull_ThenReturnsSameText() {

		String result = HyperlinkUtil.renderHyperlinkMarkup(null);

		assertThat(result, nullValue());

	}

	@Test
	public void renderHyperlinkMarkup_WhenThereIsMalformedMarkup_ThenReturnsSameText() {

		String text = "test https://www.test.co.uk|Test ";

		String result = HyperlinkUtil.renderHyperlinkMarkup(text);

		assertThat(result, equalTo(text));
	}

	@Test
	public void renderHyperlinkMarkup_WhenThereIsWellformedMarkup_ThenReturnsHyperlinksRendered() {

		String expected = "test http://test.com test <a target=\"_blank\" href=\"https://www.test.co.uk\">Test Test</a> test";

		String result = HyperlinkUtil.renderHyperlinkMarkup(TEST_TEXT);

		assertThat(result, equalTo(expected));
	}

}
