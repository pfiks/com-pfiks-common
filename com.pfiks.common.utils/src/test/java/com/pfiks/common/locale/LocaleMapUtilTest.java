/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.locale;

import com.liferay.portal.kernel.language.LanguageUtil;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mockStatic;

@RunWith(MockitoJUnitRunner.class)
public class LocaleMapUtilTest {

	private final long groupId = 1;

	private final LocaleMapUtil localeMapUtil = new LocaleMapUtil();

	private final Set<Locale> availableLocales = new HashSet<>();
	private final Set<Locale> customLocales = new HashSet<>();

	private MockedStatic<LanguageUtil> mockLanguageUtil;

	@Before
	public void setUp() throws Exception {

		mockLanguageUtil = mockStatic(LanguageUtil.class);

		availableLocales.add(Locale.ENGLISH);
		availableLocales.add(Locale.ITALIAN);
		availableLocales.add(Locale.GERMAN);

		customLocales.add(Locale.FRENCH);

	}

	@After
	public void teardown() {
		mockLanguageUtil.close();
	}


	@Test
	public void getLocaleMapForAvailableLocales_WithValidValue_ThenReturnsMapWithAllDefaultLocales() {
		String valueToAdd = "myTestValue";

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> localeMapForAvailableLocales = localeMapUtil.getLocaleMapForAvailableLocales(valueToAdd);

		assertThat(localeMapForAvailableLocales.size(), is(3));
		assertThat(localeMapForAvailableLocales.keySet(), containsInAnyOrder(Locale.ENGLISH, Locale.GERMAN, Locale.ITALIAN));

	}

	@Test
	public void getLocaleMapForAvailableLocales_WithEmptyValue_ThenReturnsEmptyMap() {
		String valueToAdd = "";

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> localeMapForAvailableLocales = localeMapUtil.getLocaleMapForAvailableLocales(valueToAdd);

		assertThat(localeMapForAvailableLocales.size(), is(0));

	}

	@Test
	public void getLocaleMapForAvailableLocales_WithNullValue_ThenReturnsEmptyMap() {
		String valueToAdd = null;

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> localeMapForAvailableLocales = localeMapUtil.getLocaleMapForAvailableLocales(valueToAdd);

		assertThat(localeMapForAvailableLocales.size(), is(0));

	}

	@Test
	public void getLocaleMap_WithValidValue_ThenReturnsMapWithDefaultLocales_AsWellAsCustomLocales() {

		String valueToAdd = "myTestValue";

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> localeMapForAvailableLocales = localeMapUtil.getLocaleMap(valueToAdd, customLocales);

		assertThat(localeMapForAvailableLocales.size(), is(4));
		assertThat(localeMapForAvailableLocales.keySet(), Matchers.containsInAnyOrder(Locale.ENGLISH, Locale.GERMAN, Locale.ITALIAN, Locale.FRENCH));

	}

	@Test
	public void getLocaleMap_WithEmptyValue_ThenReturnsEmptyMap() {

		String valueToAdd = "";

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> localeMapForAvailableLocales = localeMapUtil.getLocaleMap(valueToAdd, customLocales);

		assertThat(localeMapForAvailableLocales.size(), is(0));

	}

	@Test
	public void getLocaleMap_WithNullValue_ThenReturnsEmptyMap() {

		String valueToAdd = null;

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> localeMapForAvailableLocales = localeMapUtil.getLocaleMap(valueToAdd, customLocales);

		assertThat(localeMapForAvailableLocales.size(), is(0));


	}

	@Test
	public void filterBySystemAvailableLocales_WithNullValue_ThenReturnsEmptyMap() {

		Map<Locale, String> localeMap = null;

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> filteredMap = localeMapUtil.filterBySystemAvailableLocales(localeMap);

		assertThat(filteredMap.size(), is(0));

	}

	@Test
	public void filterBySystemAvailableLocales_WithEmptyValue_ThenReturnsEmptyMap() {

		Map<Locale, String> localeMap = new HashMap<>();

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> filteredMap = localeMapUtil.filterBySystemAvailableLocales(localeMap);

		assertThat(filteredMap.size(), is(0));

	}

	@Test
	public void filterBySystemAvailableLocales_WithValidLocaleMap_ThenReturnsFilteredMap() {

		String engValue = "engValue";
		Map<Locale, String> localeMap = Collections.singletonMap(Locale.ENGLISH, engValue);

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> filteredMap = localeMapUtil.filterBySystemAvailableLocales(localeMap);

		assertThat(filteredMap.size(), is(1));
		assertThat(filteredMap.get(Locale.ENGLISH), equalTo(engValue));

	}

	@Test
	public void filterBySystemAvailableLocales_WithValidLocaleMapContainingLocaleNotPresentInAvailableLocales_ThenReturnsFilteredMapWithoutNotPresentLocale() {

		String engValue = "engValue";
		String itValue = "itValue";
		String jpnValue = "itValue";
		Map<Locale, String> localeMap = new HashMap<>();
		localeMap.put(Locale.ENGLISH, engValue);
		localeMap.put(Locale.ITALIAN, itValue);
		localeMap.put(Locale.JAPAN, jpnValue);

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> filteredMap = localeMapUtil.filterBySystemAvailableLocales(localeMap);

		assertThat(filteredMap.size(), is(2));
		assertThat(filteredMap.get(Locale.ENGLISH), equalTo(engValue));
		assertThat(filteredMap.get(Locale.ITALIAN), equalTo(itValue));

	}

	@Test
	public void filterByGroupAvailableLocales_WithNullValueAndGroupId_ThenReturnsEmptyMap() {

		Map<Locale, String> localeMap = null;

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> filteredMap = localeMapUtil.filterByGroupAvailableLocales(localeMap, groupId);

		assertThat(filteredMap.size(), is(0));

	}

	@Test
	public void filterByGroupAvailableLocales_WithEmptyValueAndGroupId_ThenReturnsEmptyMap() {

		Map<Locale, String> localeMap = new HashMap<>();

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> filteredMap = localeMapUtil.filterByGroupAvailableLocales(localeMap, groupId);

		assertThat(filteredMap.size(), is(0));

	}

	@Test
	public void filterByGroupAvailableLocales_WithValidLocaleMapAndGroupId_ThenReturnsFilteredMap() {

		String engValue = "engValue";
		Map<Locale, String> localeMap = Collections.singletonMap(Locale.ENGLISH, engValue);

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> filteredMap = localeMapUtil.filterByGroupAvailableLocales(localeMap, groupId);

		assertThat(filteredMap.size(), is(1));
		assertThat(filteredMap.get(Locale.ENGLISH), equalTo(engValue));

	}

	@Test
	public void filterByGroupAvailableLocales_WithValidLocaleMapContainingLocaleNotPresentInAvailableLocalesAndGroupId_ThenReturnsFilteredMapWithoutNotPresentLocale() {

		String engValue = "engValue";
		String itValue = "itValue";
		String jpnValue = "itValue";
		Map<Locale, String> localeMap = new HashMap<>();
		localeMap.put(Locale.ENGLISH, engValue);
		localeMap.put(Locale.ITALIAN, itValue);
		localeMap.put(Locale.JAPAN, jpnValue);

		mockLanguageUtil.when(LanguageUtil::getAvailableLocales).thenReturn(availableLocales);
		mockLanguageUtil.when(() -> LanguageUtil.getAvailableLocales(groupId)).thenReturn(availableLocales);

		Map<Locale, String> filteredMap = localeMapUtil.filterByGroupAvailableLocales(localeMap, groupId);

		assertThat(filteredMap.size(), is(2));
		assertThat(filteredMap.get(Locale.ENGLISH), equalTo(engValue));
		assertThat(filteredMap.get(Locale.ITALIAN), equalTo(itValue));

	}

}