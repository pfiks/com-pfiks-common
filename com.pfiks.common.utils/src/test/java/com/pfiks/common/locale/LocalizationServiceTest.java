/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.locale;

import com.liferay.petra.string.StringPool;
import com.pfiks.common.locale.model.LocalizedCountry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LocalizationServiceTest {

	private LocalizationService localizationService;

	@Mock
	private LocalizedCountry mockLocalizedCountryOne;

	@Mock
	private LocalizedCountry mockLocalizedCountryTwo;

	@Mock
	private LocalizedCountry mockLocalizedCountryThree;

	private MockedStatic<LocalizedCountry> mockLocalizedCountry;

	@Before
	public void setUp() {

		mockLocalizedCountry = mockStatic(LocalizedCountry.class);

		localizationService = new LocalizationService();

	}

	@After
	public void teardown() {
		mockLocalizedCountry.close();
	}

	@Test
	public void getAvailableCountries_WhenNoError_ThenReturnsAllTheISOCountries() {

		List<String> expected = Arrays.asList(Locale.getISOCountries());

		String[] result = localizationService.getAvailableCountries();

		assertThat(Arrays.asList(result), equalTo(expected));

	}

	@Test
	public void getLocaleFromCountryCode_WhenCountryCodeIsBlank_ThenReturnsEmptyOptional() {
		Optional<Locale> result = localizationService.getLocaleFromCountryCode(StringPool.THREE_SPACES);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getLocaleFromCountryCode_WhenValidCountryCode_ThenReturnsOptionalWitlLocaleWithNoLanguageSet() {
		Optional<Locale> result = localizationService.getLocaleFromCountryCode("IT");

		assertThat(result.get().getLanguage(), isEmptyString());
	}

	@Test
	public void getLocaleFromCountryCode_WhenValidCountryCode_ThenReturnsOptionalWitlLocaleWithTheCountrySet() {
		String countryCode = "IT";

		Optional<Locale> result = localizationService.getLocaleFromCountryCode(countryCode);

		assertThat(result.get().getCountry(), equalTo(countryCode));
	}

	@Test
	public void getLocaleFromLanguageCode_WhenLanguageCodeIsNull_ThenReturnsEmptyOptional() {
		Optional<Locale> result = localizationService.getLocaleFromLanguageCode(null);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getLocaleFromLanguageCode_WhenLanguageCodeOnlyContainsLanguage_ThenReturnsOptionalWithLocaleWithOnlyLanguageSet() throws Exception {
		String languageCode = "en";

		Optional<Locale> result = localizationService.getLocaleFromLanguageCode(languageCode);

		assertThat(result.get().getLanguage(), equalTo(languageCode));
		assertThat(result.get().getCountry(), equalTo(StringPool.BLANK));
	}

	@Test
	public void getLocaleFromLanguageCode_WhenLanguageCodeContainsLanguageAndCountry_ThenReturnsOptionalWithLocaleWithLanguageAndCountrySet() throws Exception {
		String languageCode = "en_IT";

		Optional<Locale> result = localizationService.getLocaleFromLanguageCode(languageCode);

		assertThat(result.get().getLanguage(), equalTo("en"));
		assertThat(result.get().getCountry(), equalTo("IT"));
	}

	@Test
	public void getLocalizedCountries_WhenNoError_ThenReturnsTheLocalizedCountriesOrderedByNameAsc() {
		String countryCodeOne = "IT";
		String countryCodeTwo = "ES";
		String countryCodeThree = "US";
		String nameA = "nameA";
		String nameB = "nameB";
		String nameC = "nameC";
		Locale locale = Locale.GERMAN;
		Locale countryCodeOneLocale = new Locale(StringPool.BLANK, countryCodeOne);
		Locale countryCodeTwoLocale = new Locale(StringPool.BLANK, countryCodeTwo);
		Locale countryCodeThreeLocale = new Locale(StringPool.BLANK, countryCodeThree);
		String[] countryCodes = new String[]{countryCodeOne, countryCodeTwo, countryCodeThree};

		when(LocalizedCountry.getLocalizedCountry(countryCodeOneLocale, locale)).thenReturn(mockLocalizedCountryOne);
		when(LocalizedCountry.getLocalizedCountry(countryCodeTwoLocale, locale)).thenReturn(mockLocalizedCountryTwo);
		when(LocalizedCountry.getLocalizedCountry(countryCodeThreeLocale, locale)).thenReturn(mockLocalizedCountryThree);

		when(mockLocalizedCountryOne.getName()).thenReturn(nameC);
		when(mockLocalizedCountryTwo.getName()).thenReturn(nameA);
		when(mockLocalizedCountryThree.getName()).thenReturn(nameB);

		Set<LocalizedCountry> results = localizationService.getLocalizedCountries(countryCodes, locale);

		assertThat(results, contains(mockLocalizedCountryTwo, mockLocalizedCountryThree, mockLocalizedCountryOne));

	}

	@Test
	public void getLocalizedCountry_WithLocaleParams_WhenCountryLocaleIsNull_ThenReturnsEmptyOptional() {
		Locale countryLocale = null;
		Locale displayLocale = Locale.ENGLISH;

		Optional<LocalizedCountry> result = localizationService.getLocalizedCountry(countryLocale, displayLocale);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getLocalizedCountry_WithLocaleParams_WhenDisplayLocaleIsNull_ThenReturnsEmptyOptional() {
		Locale countryLocale = new Locale("fr", "IT");

		Optional<LocalizedCountry> result = localizationService.getLocalizedCountry(countryLocale, null);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getLocalizedCountry_WhenNoError_ThenReturnsOptionalWithLocalizedCountry() {

		Locale countryLocale = new Locale("fr", "IT");
		Locale displayLocale = Locale.ENGLISH;

		when(LocalizedCountry.getLocalizedCountry(countryLocale, displayLocale)).thenReturn(mockLocalizedCountryOne);

		Optional<LocalizedCountry> result = localizationService.getLocalizedCountry(countryLocale, displayLocale);

		assertThat(result.get(), sameInstance(mockLocalizedCountryOne));

	}

	@Test
	public void getLocalizedCountry_WithCountryCodeAndLocaleParams_WhenCountryCodeIsBlank_ThenReturnsEmptyOptional() {
		String countryCode = null;
		Locale displayLocale = Locale.ENGLISH;

		Optional<LocalizedCountry> result = localizationService.getLocalizedCountry(countryCode, displayLocale);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getLocalizedCountry_WithCountryCodeAndLocaleParams_WhenDisplayLocaleIsNull_ThenReturnsEmptyOptional() {
		String countryCode = null;

		Optional<LocalizedCountry> result = localizationService.getLocalizedCountry(countryCode, null);

		assertThat(result.isPresent(), equalTo(false));
	}

	@Test
	public void getLocalizedCountry_WithCountryCodeAndLocaleParams_WhenNoError_ThenReturnsOptionalWithLocalizedCountry() {

		String countryCode = "US";
		Locale countryLocale = new Locale(StringPool.BLANK, countryCode);
		Locale displayLocale = Locale.ENGLISH;

		when(LocalizedCountry.getLocalizedCountry(countryLocale, displayLocale)).thenReturn(mockLocalizedCountryOne);

		Optional<LocalizedCountry> result = localizationService.getLocalizedCountry(countryCode, displayLocale);

		assertThat(result.get(), sameInstance(mockLocalizedCountryOne));

	}

}
