/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.locale.model;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

import java.util.Locale;

import org.junit.Test;

public class LocalizedCountryTest {

	@Test
	public void getLocalizedCountry_WhenNoError_ThenReturnsLocalizedCountryWithCountryCodeFromTheLocaleCountry() {
		Locale countryLocale = new Locale("fr", "IT");
		Locale displayLocale = Locale.ENGLISH;

		LocalizedCountry result = LocalizedCountry.getLocalizedCountry(countryLocale, displayLocale);

		assertThat(result.getCode(), equalTo(countryLocale.getCountry()));
	}

	@Test
	public void getLocalizedCountry_WhenNoError_ThenReturnsLocalizedCountryWithNameFromTheLocaleCountryDisplayName() {
		Locale countryLocale = new Locale("fr", "IT");
		Locale displayLocale = Locale.ENGLISH;

		LocalizedCountry result = LocalizedCountry.getLocalizedCountry(countryLocale, displayLocale);

		assertThat(result.getName(), equalTo(countryLocale.getDisplayCountry(displayLocale)));
	}
}
