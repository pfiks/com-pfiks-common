/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 * <p>
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 * <p>
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.permission;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactory;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.UserLocalService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PermissionUtilTest {

	private static final long ADMIN_ROLE_ID = 3L;
	private static final long COMPANY_ID = 1L;
	private static final long USER_ID = 2L;

	private List<User> adminUsers;

	@Mock
	private PermissionChecker mockPermissionChecker;

	@Mock
	private PermissionCheckerFactory mockPermissionCheckerFactory;

	private MockedStatic<PermissionThreadLocal> mockPermissionThreadLocal;

	private MockedStatic<PrincipalThreadLocal> mockPrincipalThreadLocal;

	@Mock
	private Role mockRole;

	@Mock
	private RoleLocalService mockRoleLocalService;

	@Mock
	private User mockUser;

	@Mock
	private UserLocalService mockUserLocalService;

	private PermissionUtil permissionUtil;


	@Before
	public void setUp() throws Exception {

		mockPermissionThreadLocal = mockStatic(PermissionThreadLocal.class);
		mockPrincipalThreadLocal = mockStatic(PrincipalThreadLocal.class);

		permissionUtil = new PermissionUtil();
		permissionUtil.setPermissionCheckerFactory(mockPermissionCheckerFactory);
		permissionUtil.setRoleLocalService(mockRoleLocalService);
		permissionUtil.setUserLocalService(mockUserLocalService);

		adminUsers = new ArrayList<>();

	}

	@After
	public void teardown() {

		mockPermissionThreadLocal.close();
		mockPrincipalThreadLocal.close();

	}


	@Test
	public void runAsCompanyAdmin_WhenAtLeastOneAdminUserIsFound_ThenSetsTheFirstCompanyAdminUserFoundInPermissionThread() throws Exception {
		mockAdminRoleDetails();
		mockPermissionCheckerForUser();
		adminUsers.add(mockUser);
		when(mockUserLocalService.getRoleUsers(ADMIN_ROLE_ID)).thenReturn(adminUsers);

		permissionUtil.runAsCompanyAdmin(COMPANY_ID);

		verifyUserIsSetInPermissionChecker();
	}

	@Test
	public void runAsCompanyAdmin_WhenNoCompanyAdminsAreFound_ThenNoActionIsPerformed() throws Exception {
		mockAdminRoleDetails();
		when(mockUserLocalService.getRoleUsers(ADMIN_ROLE_ID)).thenReturn(adminUsers);

		permissionUtil.runAsCompanyAdmin(COMPANY_ID);

		verifyNoChangesInPermissions();
	}

	@Test
	public void runAsUser_WithUserIdParamGreaterThanZero_ThenSetsTheUserInPermissionThread() throws Exception {
		mockPermissionCheckerForUser();
		when(mockUserLocalService.getUser(USER_ID)).thenReturn(mockUser);

		permissionUtil.runAsUser(USER_ID);

		verifyUserIsSetInPermissionChecker();
	}

	@Test
	public void runAsUser_WithUserIdParamNotGreaterThanZero_ThenDoesNotPerformAnyActions() throws Exception {
		permissionUtil.runAsUser(0L);
		verify(mockUserLocalService, never()).getUser(anyLong());
		verifyNoChangesInPermissions();
	}

	@Test
	public void runAsUser_WithUserParam_ThenSetsTheUserInPermissionThread() throws Exception {
		mockPermissionCheckerForUser();

		permissionUtil.runAsUser(mockUser);

		verifyUserIsSetInPermissionChecker();
	}


	private void mockAdminRoleDetails() throws PortalException {
		when(mockRole.getRoleId()).thenReturn(ADMIN_ROLE_ID);
		when(mockRoleLocalService.getRole(COMPANY_ID, RoleConstants.ADMINISTRATOR)).thenReturn(mockRole);
	}

	private void mockPermissionCheckerForUser() {
		when(mockUser.getUserId()).thenReturn(USER_ID);
		when(mockPermissionCheckerFactory.create(mockUser)).thenReturn(mockPermissionChecker);
	}

	private void verifyNoChangesInPermissions() {

		mockPrincipalThreadLocal.verify(() -> PrincipalThreadLocal.setName(anyLong()), never());
		mockPermissionThreadLocal.verify(() -> PermissionThreadLocal.setPermissionChecker(any(PermissionChecker.class)), never());

	}

	private void verifyUserIsSetInPermissionChecker() {

		mockPrincipalThreadLocal.verify(() -> PrincipalThreadLocal.setName(USER_ID), times(1));
		mockPermissionThreadLocal.verify(() -> PermissionThreadLocal.setPermissionChecker(mockPermissionChecker), times(1));

	}

}
