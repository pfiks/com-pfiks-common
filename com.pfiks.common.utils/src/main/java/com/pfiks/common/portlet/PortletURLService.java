/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.portlet;

import java.util.Map;
import java.util.Optional;

import javax.portlet.PortletURL;
import javax.portlet.RenderResponse;

import com.liferay.portal.kernel.theme.ThemeDisplay;

/**
 * The PortletURLService interface.
 */
public interface PortletURLService {

	/**
	 * Gets the portlet URL containing the supplied parameters.
	 *
	 * @param renderResponse the render response
	 * @param parameterMap a map containing parameters to be included in the
	 *            portletURL
	 * @return the portlet URL
	 */
	public PortletURL getPortletURL(RenderResponse renderResponse, Map<String, String> parameterMap);

	/**
	 * Gets a redirect to go back to the current page, taking into account the
	 * current portlet lifecycle.
	 * 
	 * @param themeDisplay the ThemeDisplay
	 * @param encode whether or not to encode the result
	 * @return the redirect URL
	 */
	public Optional<String> getRedirectURL(ThemeDisplay themeDisplay, boolean encode);

}
