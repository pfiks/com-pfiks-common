/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.portlet;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;
import java.util.Optional;

import javax.portlet.MutableRenderParameters;
import javax.portlet.PortletURL;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.StringUtil;

@Component(immediate = true, service = PortletURLService.class)
public class PortletURLServiceImpl implements PortletURLService {

	private static final Log LOG = LogFactoryUtil.getLog(PortletURLServiceImpl.class);

	@Override
	public PortletURL getPortletURL(RenderResponse renderResponse, Map<String, String> parameters) {
		PortletURL portletURL = renderResponse.createRenderURL();

		if (parameters != null) {

			for (Map.Entry<String, String> parameterMapEntry : parameters.entrySet()) {
				MutableRenderParameters renderParameters = portletURL.getRenderParameters();
				renderParameters.setValue(parameterMapEntry.getKey(), parameterMapEntry.getValue());
			}

		}
		return portletURL;
	}

	@Override
	public Optional<String> getRedirectURL(ThemeDisplay themeDisplay, boolean encode) {
		String redirect = null;
		try {
			if (StringUtil.equalsIgnoreCase(themeDisplay.getLifecycle(), "2")) {
				// just redirect to current page, without params
				redirect = PortalUtil.getLayoutRelativeURL(themeDisplay.getLayout(), themeDisplay);
			} else {
				redirect = themeDisplay.getURLCurrent();
			}
		} catch (Exception e) {
			LOG.warn("Couldn't build redirect URL for " + themeDisplay.getURLCurrent());
		}
		if (encode && redirect != null) {
			redirect = getEncodedURL(redirect);
		}
		return Optional.ofNullable(redirect);
	}

	private String getEncodedURL(String urlToEncode) {
		try {
			return URLEncoder.encode(urlToEncode, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			LOG.warn("Exception encoding currentURL", e);
		}
		return urlToEncode;
	}
}
