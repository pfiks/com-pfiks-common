/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.domain;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = EmailDomainChecker.class)
public class EmailDomainChecker {

	public Set<String> getDomainPartsFromEmail(String emailAddress) {
		Set<String> results = new HashSet<>();
		if (Validator.isNotNull(emailAddress)) {
			int indexOfAt = emailAddress.indexOf(StringPool.AT);
			String domain = emailAddress.substring(indexOfAt + 1);
			results.add(StringUtils.trimToEmpty(domain));

			while (domain.indexOf(StringPool.PERIOD) != -1) {
				int indexOfPeriod = domain.indexOf(StringPool.PERIOD);
				domain = domain.substring(indexOfPeriod + 1, domain.length());
				results.add(StringUtils.trimToEmpty(domain.toLowerCase()));
			}
		}
		return results;
	}

	public boolean isEmailValidForDomain(String emailAddress, String domain) {
		domain = StringUtils.trimToEmpty(domain);
		if (Validator.isNotNull(emailAddress) && Validator.isNotNull(domain)) {
			String emailAddressDomain = getEmailDomainFromEmailAddress(emailAddress);
			Set<String> domainPartsFromEmail = getDomainPartsFromEmail(emailAddressDomain);
			return checkDomainMatchesEmailDomains(emailAddressDomain, domainPartsFromEmail, domain);
		}
		return false;
	}

	public boolean isEmailValidForDomains(String emailAddress, String[] domains) {
		if (ArrayUtil.isNotEmpty(domains) && Validator.isNotNull(emailAddress)) {
			String emailAddressDomain = getEmailDomainFromEmailAddress(emailAddress);
			Set<String> domainPartsFromEmail = getDomainPartsFromEmail(emailAddressDomain);

			for (String domain : domains) {
				if (checkDomainMatchesEmailDomains(emailAddressDomain, domainPartsFromEmail, domain)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean checkDomainMatchesEmailDomains(String emailAddressDomain, Set<String> domainPartsFromEmail, String domain) {
		domain = StringUtils.trimToEmpty(domain);
		emailAddressDomain = StringUtils.trimToEmpty(emailAddressDomain);
		if (domain.startsWith(StringPool.AT)) {
			return domain.equalsIgnoreCase(StringPool.AT + emailAddressDomain);
		} else {
			return domainPartsFromEmail.contains(domain.toLowerCase());
		}
	}

	private String getEmailDomainFromEmailAddress(String emailAddress) {
		return emailAddress.split(StringPool.AT)[1];
	}
}
