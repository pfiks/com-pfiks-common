/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.internal;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;

@Component(immediate = true, service = CommonUtilsService.class)
public class CommonUtilsService {

	public String getAvailableLocales(Group group, UnicodeProperties typeSettingsProperties) {
		return Validator.isNotNull(typeSettingsProperties.getProperty(PropsKeys.LOCALES)) ? typeSettingsProperties.getProperty(PropsKeys.LOCALES) : String.join(",", group.getAvailableLanguageIds());
	}

	public String getLanguageId(Group group, UnicodeProperties typeSettingsProperties) {
		return Validator.isNotNull(typeSettingsProperties.getProperty("languageId")) ? typeSettingsProperties.getProperty("languageId") : group.getDefaultLanguageId();
	}

}
