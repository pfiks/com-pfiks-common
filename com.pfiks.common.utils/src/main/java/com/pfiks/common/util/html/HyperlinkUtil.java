/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.util.html;

import java.util.regex.Pattern;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;

public class HyperlinkUtil {

	private static final String BASE_REGEX = "((https|http):\\/\\/\\S+)";
	private static final String HYPERLINK_REGEX = "( " + BASE_REGEX + " )";
	private static final String HYPERLINK_MARKUP_REGEX = "((\\[" + BASE_REGEX + ")(\\|(([ \\w ]*)\\])))";
	private static final String HYPERLINK_HTML_TEMPLATE = StringPool.SPACE + "<a target=\"_blank\" href=\"$2\">$2</a>" + StringPool.SPACE;
	private static final String HYPERLINK_MARKUP_HTML_TEMPLATE = "<a target=\"_blank\" href=\"$3\">$7</a>";

	private static Pattern hyperlinkPattern = Pattern.compile(HYPERLINK_REGEX);
	private static Pattern hyperlinkMarkupPattern = Pattern.compile(HYPERLINK_MARKUP_REGEX);

	private HyperlinkUtil() {

	}

	public static String convertToHyperlink(final String text) {
		return Validator.isNotNull(text) ? hyperlinkPattern.matcher(text).replaceAll(HYPERLINK_HTML_TEMPLATE) : text;
	}

	public static String decorateHyperlinks(final String text) {
		return renderHyperlinkMarkup(convertToHyperlink(text));
	}

	public static String renderHyperlinkMarkup(final String text) {
		return Validator.isNotNull(text) ? hyperlinkMarkupPattern.matcher(text).replaceAll(HYPERLINK_MARKUP_HTML_TEMPLATE) : text;
	}
}
