/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.locale;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.osgi.service.component.annotations.Component;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.common.locale.model.LocalizedCountry;

@Component(immediate = true, service = LocalizationService.class)
public class LocalizationService {

	public String[] getAvailableCountries() {
		return Locale.getISOCountries();
	}

	public Optional<Locale> getLocaleFromCountryCode(String countryCode) {
		Locale result = null;
		if (Validator.isNotNull(countryCode)) {
			result = new Locale(StringPool.BLANK, countryCode);
		}
		return Optional.ofNullable(result);
	}

	public Optional<Locale> getLocaleFromLanguageCode(String languageCode) {
		Locale result = null;
		if (Validator.isNotNull(languageCode)) {
			if (languageCode.contains("_")) {
				String[] localeArray = languageCode.split("_");
				result = new Locale(localeArray[0], localeArray[1]);
			} else {
				result = new Locale(languageCode);
			}
		}
		return Optional.ofNullable(result);
	}

	public Set<LocalizedCountry> getLocalizedCountries(String[] countryCodes, Locale locale) {
		Set<LocalizedCountry> unorderedCountries = new HashSet<>();
		for (String code : countryCodes) {
			Optional<LocalizedCountry> localizedCountryForLocale = getLocalizedCountry(code, locale);
			if (localizedCountryForLocale.isPresent()) {
				unorderedCountries.add(localizedCountryForLocale.get());
			}
		}
		return unorderedCountries.stream().sorted((e1, e2) -> e1.getName().compareTo(e2.getName())).collect(Collectors.toCollection(LinkedHashSet::new));
	}

	public Optional<LocalizedCountry> getLocalizedCountry(Locale country, Locale display) {
		LocalizedCountry result = null;
		if (Validator.isNotNull(country) && Validator.isNotNull(display)) {
			result = LocalizedCountry.getLocalizedCountry(country, display);
		}
		return Optional.ofNullable(result);
	}

	public Optional<LocalizedCountry> getLocalizedCountry(String countryCode, Locale locale) {
		LocalizedCountry result = null;
		if (Validator.isNotNull(countryCode) && Validator.isNotNull(locale)) {
			result = LocalizedCountry.getLocalizedCountry(new Locale(StringPool.BLANK, countryCode), locale);
		}
		return Optional.ofNullable(result);
	}
}
