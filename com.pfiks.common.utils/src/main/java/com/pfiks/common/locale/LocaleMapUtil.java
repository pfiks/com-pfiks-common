/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.locale;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * This class provides utility methods for creating Maps of values for different
 * locales
 *
 */
@Component(immediate = true, service = LocaleMapUtil.class)
public class LocaleMapUtil {

	public ResourceBundle getResourceBundle(Locale locale) {
		return ResourceBundle.getBundle("content/Language", locale);
	}

	/**
	 * Creates a Map containing the valueToAdd for any Locale available in the
	 * company
	 *
	 * @param valueToAdd the value to add to the existing locale map
	 * @return a map containing any locale available
	 */
	public Map<Locale, String> getLocaleMapForAvailableLocales(String valueToAdd) {
		Map<Locale, String> map = new HashMap<>();
		if (Validator.isNotNull(valueToAdd)) {
			Set<Locale> availableLocales = LanguageUtil.getAvailableLocales();
			for (Locale locale : availableLocales) {
				map.put(locale, valueToAdd);
			}
		}
		return map;
	}

	/**
	 * Creates a Map containing the valueToAdd for any Locale available in the
	 * company plus any locale specified in contentLocales
	 *
	 * @param valueToAdd the value to add to the existing locale map
	 * @param customLocales custom locales to be added
	 * @return a map containing any locale available plus the custom specified
	 */
	public Map<Locale, String> getLocaleMap(String valueToAdd, Set<Locale> customLocales) {
		Map<Locale, String> map = getLocaleMapForAvailableLocales(valueToAdd);
		if (Validator.isNotNull(valueToAdd)) {
			for (Locale locale : customLocales) {
				map.put(locale, valueToAdd);
			}
		}
		return map;
	}

	/**
	 * Filters given locale map based on locales available in the company
	 *
	 * @param localeMap map to be filtered by available locales
	 * @return a locale map filtered by company available locales
	 */
	public Map<Locale, String> filterBySystemAvailableLocales(Map<Locale, String> localeMap) {
		Set<Locale> availableLocales = LanguageUtil.getAvailableLocales();
		return filterByAvailableLocales(localeMap, availableLocales);
	}

	/**
	 * Filters given locale map based on locales available in the group
	 *
	 * @param localeMap map to be filtered by available locales
	 * @param groupId of the group of which the available locales will be used
	 * @return a locale map filtered by group available locales
	 */
	public Map<Locale, String> filterByGroupAvailableLocales(Map<Locale, String> localeMap, long groupId) {
		Set<Locale> availableLocales = LanguageUtil.getAvailableLocales(groupId);
		return filterByAvailableLocales(localeMap, availableLocales);
	}

	private Map<Locale, String> filterByAvailableLocales(Map<Locale, String> localeMap, Set<Locale> availableLocales) {
		Map<Locale, String> map = new HashMap<>();
		if (Validator.isNotNull(localeMap) && !localeMap.isEmpty()) {
			for (Locale locale : availableLocales) {
				String valueToAdd = localeMap.get(locale);
				if (Validator.isNotNull(valueToAdd)) {
					map.put(locale, valueToAdd);
				}
			}
		}
		return map;
	}

}
