/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.servicecontext;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.model.role.RoleConstants;
import com.liferay.portal.kernel.security.permission.ActionKeys;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.permission.ModelPermissions;

/**
 * This class provides utility methods for creating serviceContexts
 *
 */
@Component(immediate = true, service = ServiceContextHelper.class)
public class ServiceContextHelper {

	/**
	 * Creates a new service context object with only the company id given
	 * without any extra configuration
	 *
	 * @param companyId the companId
	 * @return the configured serviceContext
	 */
	public ServiceContext getServiceContext(long companyId) {
		ServiceContext serviceContext = new ServiceContext();
		serviceContext.setCompanyId(companyId);
		return serviceContext;
	}

	/**
	 * Creates a new service context object with only the company id given
	 * without any extra configuration and sets categoryIds
	 *
	 * @param companyId the companId
	 * @param assetCategoryIds the assetCategoryIds
	 * @return the configured serviceContext
	 */
	public ServiceContext getServiceContext(long companyId, long[] assetCategoryIds) {
		ServiceContext serviceContext = getServiceContext(companyId);
		serviceContext.setAssetCategoryIds(assetCategoryIds);
		return serviceContext;
	}

	/**
	 * Creates a new service context object with guestPermission = VIEW,
	 * groupPermission = VIEW
	 *
	 * @param companyId the companyId
	 * @param groupId the groupId
	 * @param userId the userId
	 * @return the configured serviceContext
	 */
	public ServiceContext getServiceContext(long companyId, long groupId, long userId) {
		final ServiceContext serviceContext = getServiceContext(companyId);
		serviceContext.setScopeGroupId(groupId);
		serviceContext.setUserId(userId);
		serviceContext.setAddGroupPermissions(true);
		serviceContext.setAddGuestPermissions(true);
		ModelPermissions modelPermissions = serviceContext.getModelPermissions();
		if (modelPermissions!=null) {
			modelPermissions.addRolePermissions(RoleConstants.PLACEHOLDER_DEFAULT_GROUP_ROLE, new String[] { ActionKeys.VIEW });
			modelPermissions.addRolePermissions(RoleConstants.GUEST, new String[] { ActionKeys.VIEW });
		}
		serviceContext.setModelPermissions(modelPermissions);
		return serviceContext;
	}

	/**
	 * Creates a new service context object with guestPermission = VIEW,
	 * groupPermission = VIEW and sets categoryIds
	 *
	 * @param companyId the companyId
	 * @param groupId the groupId
	 * @param userId the userId
	 * @param assetCategoryIds the assetCategoryIds
	 * @return the configured serviceContext
	 */
	public ServiceContext getServiceContext(long companyId, long groupId, long userId, long[] assetCategoryIds) {
		final ServiceContext serviceContext = getServiceContext(companyId, groupId, userId);
		serviceContext.setAssetCategoryIds(assetCategoryIds);
		return serviceContext;
	}

}
