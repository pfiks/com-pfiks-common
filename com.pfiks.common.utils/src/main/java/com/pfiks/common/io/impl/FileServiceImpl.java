/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.io.impl;

import java.io.InputStream;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.pfiks.common.io.FileService;
import com.pfiks.common.io.InputOuputUtil;

@Component(immediate = true, service = FileService.class)
public class FileServiceImpl implements FileService {

	private InputOuputUtil inputOutputUtil;

	@Override
	public InputStream loadFile(Class<?> clazz, String fileName) {
		return clazz.getClassLoader().getResourceAsStream(fileName);
	}

	@Override
	public Map<String, InputStream> loadFiles(Class<?> clazz, String path, String extension) {
		Map<String, InputStream> results = new HashMap<>();
		Bundle bundle = FrameworkUtil.getBundle(clazz);
		Enumeration<URL> filesToImport = bundle.findEntries(path, extension, true);
		if (filesToImport != null) {
			while (filesToImport.hasMoreElements()) {
				URL nextElement = filesToImport.nextElement();
				String pathForFile = nextElement.getPath();
				String fileName = inputOutputUtil.getCleanFileNameFromFilePath(pathForFile);
				results.put(fileName, loadFile(clazz, pathForFile));
			}
		}
		return results;
	}

	@Reference
	protected void setInputOutputUtil(InputOuputUtil inputOutputUtil) {
		this.inputOutputUtil = inputOutputUtil;
	}

}