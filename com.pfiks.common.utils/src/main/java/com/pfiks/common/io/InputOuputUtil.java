/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.io;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.model.Group;
import com.liferay.portal.kernel.util.UnicodeProperties;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.common.exception.CommonUtilsException;
import com.pfiks.common.internal.CommonUtilsService;

/**
 * This class provides utility methods for reading files
 *
 */
@Component(immediate = true, service = InputOuputUtil.class)
public class InputOuputUtil {

	private final static String LOCALE_DEFAULT = "\\[\\$LOCALE_DEFAULT\\$\\]";
	private final static String AVAILABLE_LOCALES = "\\[\\$AVAILABLE_LOCALES\\$\\]";

	private CommonUtilsService commonUtilsService;

	/**
	 * Gets the content of the inputStream as a string, using UTF_8 as charset
	 *
	 * @param inputStream the input stream
	 * @return String representing the content of the inputStream
	 * @throws CommonUtilsException if any exception occurs while reading the
	 *             content of the InputStream
	 */
	public String getStringFromInputStream(InputStream inputStream) throws CommonUtilsException {
		try {
			return IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
		} catch (Exception e) {
			throw new CommonUtilsException("Exception retrieving content from inputStream", e);
		}
	}

	/**
	 * Gets the content of the inputStream as a string, using UTF_8 as charset
	 * with default language ID and available languages set.
	 *
	 * @param inputStream the input stream
	 * @param languageId the default language code
	 * @param availableLanguages the available languages
	 * @return String representing the content of the inputStream
	 * @throws CommonUtilsException if any exception occurs while reading the
	 *             content of the InputStream
	 */
	public String getLocalisedStringFromInputStream(InputStream inputStream, String languageId, String availableLanguages) throws CommonUtilsException {
		try {
			String content = IOUtils.toString(inputStream, StandardCharsets.UTF_8.name());
			if (Validator.isNotNull(languageId) && Validator.isNotNull(availableLanguages)) {
				return content.replaceAll(LOCALE_DEFAULT, languageId).replaceAll(AVAILABLE_LOCALES, availableLanguages);
			}
			return content;
		} catch (Exception e) {
			throw new CommonUtilsException("Exception retrieving content from inputStream", e);
		}
	}

	/**
	 * Gets the content of the inputStream as a string, using UTF_8 as charset
	 * with default language ID and available languages set.
	 *
	 * @param group the group
	 * @param inputStream the input stream
	 * @return String representing the content of the inputStream
	 * @throws CommonUtilsException if any exception occurs while reading the
	 *             content of the InputStream
	 */
	public String getLocalizedContentFromInputStream(Group group, InputStream inputStream) throws CommonUtilsException {
		UnicodeProperties typeSettingsProperties = group.getTypeSettingsProperties();
		String languageId = commonUtilsService.getLanguageId(group, typeSettingsProperties);
		String availableLanguages = commonUtilsService.getAvailableLocales(group, typeSettingsProperties);

		return getLocalisedStringFromInputStream(inputStream, languageId, availableLanguages);
	}

	/**
	 * Give a full file name, it returns a friendlyName by converting any space
	 * in dash and then the whole string to uppercase
	 *
	 * @param fileName the file name
	 * @return the updated String
	 */
	public String getKeyFromFileName(String fileName) {
		String key = StringUtils.replaceEachRepeatedly(fileName, new String[] { StringPool.SPACE }, new String[] { StringPool.DASH });
		return key.toUpperCase();
	}

	/**
	 * Given a file path, it returns the file name evaluated as between the last
	 * forward slash and the last period
	 *
	 * @param pathForFile the string to update
	 * @return the cleared file name
	 */
	public String getCleanFileNameFromFilePath(String pathForFile) {
		String fileName = StringUtils.substringAfterLast(pathForFile, StringPool.FORWARD_SLASH);
		if (Validator.isNotNull(fileName)) {
			return StringUtils.substringBeforeLast(fileName, StringPool.PERIOD);
		} else {
			return StringUtils.substringBeforeLast(pathForFile, StringPool.PERIOD);
		}
	}

	@Reference
	protected void setCommonUtilsService(CommonUtilsService commonUtilsService) {
		this.commonUtilsService = commonUtilsService;
	}

}
