/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.permission;

import java.util.List;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.security.auth.PrincipalThreadLocal;
import com.liferay.portal.kernel.security.permission.PermissionChecker;
import com.liferay.portal.kernel.security.permission.PermissionCheckerFactory;
import com.liferay.portal.kernel.security.permission.PermissionThreadLocal;
import com.liferay.portal.kernel.service.RoleLocalService;
import com.liferay.portal.kernel.service.UserLocalService;

@Component(immediate = true, service = PermissionUtil.class)
public class PermissionUtil {

	private RoleLocalService roleLocalService;
	private UserLocalService userLocalService;
	private PermissionCheckerFactory permissionCheckerFactory;

	public void runAsUser(long userId) throws Exception {
		if (userId > 0) {
			runAsUser(userLocalService.getUser(userId));
		}
	}

	public void runAsCompanyAdmin(long companyId) throws Exception {
		final Role adminRole = roleLocalService.getRole(companyId, RoleConstants.ADMINISTRATOR);
		final List<User> adminUsers = userLocalService.getRoleUsers(adminRole.getRoleId());
		if (!adminUsers.isEmpty()) {
			runAsUser(adminUsers.get(0));
		}
	}

	public void runAsUser(User user) throws Exception {
		PrincipalThreadLocal.setName(user.getUserId());
		PermissionChecker permissionChecker = permissionCheckerFactory.create(user);
		PermissionThreadLocal.setPermissionChecker(permissionChecker);
	}

	@Reference
	public void setRoleLocalService(RoleLocalService roleLocalService) {
		this.roleLocalService = roleLocalService;
	}

	@Reference
	public void setUserLocalService(UserLocalService userLocalService) {
		this.userLocalService = userLocalService;
	}

	@Reference
	public void setPermissionCheckerFactory(PermissionCheckerFactory permissionCheckerFactory) {
		this.permissionCheckerFactory = permissionCheckerFactory;
	}

}
