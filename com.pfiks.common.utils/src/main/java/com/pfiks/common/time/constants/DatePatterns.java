/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.time.constants;

public class DatePatterns {

	public static final String DAY_MON_YEAR_TIME = "dd-MMM-yyyy HH:mm";

	public static final String DAY_MONTH_YEAR_TIME = "dd MMMM YYYY hh:mma";

	public static final String DAY_MONTH_YEAR_TIME_24 = "dd MMMM YYYY HH:mm";

	public static final String DATE_TIME_SHORT_FORMAT = "dd/MM/yy HH:mm";

	public static final String DAY_MONTH_YEAR = "dd MMMM YYYY";

	public static final String MONTH_YEAR = "MMMM YYYY";

	public static final String DAY_MONTH = "dd MMMM";

	public static final String MONTH = "MMMM";

	private DatePatterns() {
	}

}
