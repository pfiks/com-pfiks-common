/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.time;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.TimeZone;

import javax.portlet.PortletRequest;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.pfiks.common.time.model.DateTimeSelection;

@Component(immediate = true, service = DateTimeSelectionService.class)
public class DateTimeSelectionService {

	public DateTimeSelection getDateTimeSelectionWithHours(TimeZone timeZone, int hoursToAdd) {
		Calendar currentDateTime = Calendar.getInstance(timeZone);
		if (hoursToAdd > 0) {
			currentDateTime.add(Calendar.HOUR, hoursToAdd);
		}
		return new DateTimeSelection(currentDateTime);
	}

	public DateTimeSelection getDateTimeSelectionWithMonths(TimeZone timeZone, int monthsToAdd) {
		Calendar currentDateTime = Calendar.getInstance(timeZone);
		if (monthsToAdd > 0) {
			currentDateTime.add(Calendar.MONTH, monthsToAdd);
		}
		return new DateTimeSelection(currentDateTime);
	}

	public DateTimeSelection getDateTimeSelection(TimeZone timeZone) {
		Calendar currentDateTime = Calendar.getInstance(timeZone);
		return new DateTimeSelection(currentDateTime);
	}

	public DateTimeSelection getDateTimeSelectionFromMillis(long millis, TimeZone timeZone) {
		Calendar currentDateTime = Calendar.getInstance(timeZone);
		currentDateTime.setTimeInMillis(millis);
		return new DateTimeSelection(currentDateTime);
	}

	public DateTimeSelection getDateTimeSelectionFromDate(Date date, TimeZone timeZone) {
		Calendar currentDateTime = Calendar.getInstance(timeZone);
		currentDateTime.setTime(date);
		return new DateTimeSelection(currentDateTime);
	}

	public DateTimeSelection getDateTimeSelectionFromRequest(PortletRequest portletRequest, String dateParamPrefixKey) {
		int hour = ParamUtil.getInteger(portletRequest, dateParamPrefixKey + "Hour", 0);
		int amOrPm = ParamUtil.getInteger(portletRequest, dateParamPrefixKey + "AMorPM", Calendar.AM);
		if (Calendar.PM == amOrPm) {
			hour += 12;
		}

		DateTimeSelection result = new DateTimeSelection();
		result.setYear(ParamUtil.getInteger(portletRequest, dateParamPrefixKey + "Year"));
		result.setMonth(ParamUtil.getInteger(portletRequest, dateParamPrefixKey + "Month"));
		result.setDay(ParamUtil.getInteger(portletRequest, dateParamPrefixKey + "Day"));
		result.setAmORpm(amOrPm);
		result.setHour(hour);
		result.setMinute(ParamUtil.getInteger(portletRequest, dateParamPrefixKey + "Minute", 0));
		return result;
	}

	public Date getDateFromDateTimeSelection(DateTimeSelection dateTimeSelection, TimeZone timeZone) throws PortalException {
		return PortalUtil.getDate(dateTimeSelection.getMonth(), dateTimeSelection.getDay(), dateTimeSelection.getYear(), dateTimeSelection.getHour(), dateTimeSelection.getMinute(), timeZone,
				PortalException.class);
	}

	public Optional<Date> getDateFromDateTimeSelection(boolean includeTime, DateTimeSelection dateTimeSelection, TimeZone timeZone) {
		try {
			if (includeTime) {
				return Optional.of(PortalUtil.getDate(dateTimeSelection.getMonth(), dateTimeSelection.getDay(), dateTimeSelection.getYear(), dateTimeSelection.getHour(), dateTimeSelection.getMinute(),
						timeZone, PortalException.class));
			} else {
				return Optional.of(PortalUtil.getDate(dateTimeSelection.getMonth(), dateTimeSelection.getDay(), dateTimeSelection.getYear(), timeZone, PortalException.class));
			}
		} catch (PortalException e) {
			return Optional.empty();
		}
	}

}
