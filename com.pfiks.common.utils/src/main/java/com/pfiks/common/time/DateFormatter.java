/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.time;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.liferay.petra.string.StringPool;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.pfiks.common.locale.LocaleMapUtil;
import com.pfiks.common.time.constants.DatePatterns;

/**
 * This class provides utility methods for dates.
 *
 * @author Chris Newton
 *
 */
@Component(immediate = true, service = DateFormatter.class)
public class DateFormatter {

	private static final Log LOG = LogFactoryUtil.getLog(DateFormatter.class);

	private static final ZoneId ZONE_UTC = ZoneId.of("UTC");

	private static final DateTimeFormatter DATE_TIME_FORMATTER_FULL_VALUE = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

	private static final DateTimeFormatter DATE_TIME_FORMATTER_COMPACT_VALUE = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");

	private LocaleMapUtil localeMapUtil;

	/**
	 * Gets the formatted date.
	 *
	 * @param date the date
	 * @param timeZoneId the time zone id @see <a href=
	 *            "http://docs.oracle.com/javase/7/docs/api/java/util/TimeZone.html#getAvailableIDs%28%29">TimeZone.getAvailableIDs()</a>
	 * @param pattern the pattern of the formatted date
	 * @return the formatted date
	 */
	public String getFormattedDate(Date date, String timeZoneId, String pattern) {
		return getFormattedInstant(date.toInstant(), timeZoneId, pattern);
	}

	/**
	 * Gets the formatted instant.
	 *
	 * @param instant the instant
	 * @param timeZoneId the time zone id @see <a href=
	 *            "http://docs.oracle.com/javase/7/docs/api/java/util/TimeZone.html#getAvailableIDs%28%29">TimeZone.getAvailableIDs()</a>
	 * @param pattern the pattern of the formatted date
	 * @return the formatted date
	 */
	public String getFormattedInstant(Instant instant, String timeZoneId, String pattern) {
		return getFormattedInstant(instant, timeZoneId, null, pattern);
	}
	
	/**
	 * Gets the formatted instant.
	 *
	 * @param instant the instant
	 * @param timeZoneId the time zone id @see <a href=
	 *            "http://docs.oracle.com/javase/7/docs/api/java/util/TimeZone.html#getAvailableIDs%28%29">TimeZone.getAvailableIDs()</a>
	 * @param locale the locale
	 * @param pattern the pattern of the formatted date
	 * @return the formatted date
	 */
	public String getFormattedInstant(Instant instant, String timeZoneId, Locale locale, String pattern) {
		ZoneId zoneId = ZoneId.of(timeZoneId);
		ZonedDateTime dateZoneDateTime = instant.atZone(zoneId);
		LocalDateTime localDateTime = dateZoneDateTime.toLocalDateTime();
		DateTimeFormatter formatter = Validator.isNull(locale) ? DateTimeFormatter.ofPattern(pattern) : DateTimeFormatter.ofPattern(pattern, locale);
		return formatter.format(localDateTime);
	}

	/**
	 * Formats the instant using the pattern yyyy-MM-dd'T'HH:mm:ss.SSS'Z', using
	 * UTC timezone
	 *
	 * @param instant the instant
	 * @return the formatted string, or an empty string if the instant was null
	 */
	public String getFormattedInstant(Instant instant) {
		String formattedDate = StringPool.BLANK;
		if (Validator.isNotNull(instant)) {
			LocalDateTime localDateTimeValue = LocalDateTime.ofInstant(instant, ZONE_UTC);
			formattedDate = localDateTimeValue.format(DATE_TIME_FORMATTER_FULL_VALUE.withZone(ZONE_UTC));
		}
		return formattedDate;
	}

	/**
	 * Given a string representing a date in the format 'yyyyMMddHHmmss', it
	 * returns a new string formatting the date the pattern
	 * yyyy-MM-dd'T'HH:mm:ss.SSS'Z', using UTC timezone
	 *
	 * @param compactDateValue the date value in the format 'yyyyMMddHHmmss'
	 * @return the formatted string, or an empty string if the compactDateValue
	 *         was null or not a valid numeric value
	 */
	public String getLongFormatDateFromCompactDate(String compactDateValue) {
		String result = "";
		if (Validator.isNotNull(compactDateValue) && Validator.isNumber(compactDateValue)) {
			Instant instant = Instant.from(DATE_TIME_FORMATTER_COMPACT_VALUE.withZone(ZONE_UTC).parse(compactDateValue));
			result = getFormattedInstant(instant);
		}
		return result;
	}

	/**
	 * Given a string representing a date in the format
	 * 'yyyy-MM-dd'T'HH:mm:ss.SSS'Z', it returns a new instant, using UTC
	 * timezone
	 *
	 * @param longDateValue the date value in the format
	 *            'yyyy-MM-dd'T'HH:mm:ss.SSS'Z'
	 * @return the Instant for the date, using UTC timezone. null if any
	 *         exception occurs
	 */
	public Instant getInstantFromLongFormatString(String longDateValue) {
		Instant result = null;
		try {
			if (Validator.isNotNull(longDateValue)) {
				result = Instant.from(DATE_TIME_FORMATTER_FULL_VALUE.withZone(ZONE_UTC).parse(longDateValue));
			}
		} catch (Exception e) {
			LOG.warn("Exception retrieving Instant from longDateValue: " + longDateValue + " - " + e.getMessage());
		}
		return result;
	}

	/**
	 * Given an Instant, it evaluates the friendlyDate following the pattern: if
	 * same year: dd MMMM if different year: dd MMMM YYYY
	 *
	 * The Instant is first parsed into the TimeZone and then compared to
	 * today's date evaluated in same timeZone
	 *
	 * @param instant the instant
	 * @param timeZoneId the time zone ID
	 * @param locale locale
	 * @return the friendly date, or empty string if the date is empty
	 */
	public String getFriendlyDate(Instant instant, String timeZoneId, Locale locale) {
		if (Validator.isNotNull(instant)) {
			ZoneId zoneId = ZoneId.of(timeZoneId);
			LocalDateTime timeToCheck = LocalDateTime.ofInstant(instant, zoneId);
			LocalDateTime nowTime = LocalDateTime.now(zoneId);

			Period period = Period.between(timeToCheck.toLocalDate(), nowTime.toLocalDate());

			if (isDifferentYear(period, timeToCheck, nowTime)) {
				// Different year - Return Month and Year
				return getFormattedInstant(instant, timeZoneId, locale, DatePatterns.DAY_MONTH_YEAR);
			} else {
				// Same year - Return day and Month
				return getFormattedInstant(instant, timeZoneId, locale, DatePatterns.DAY_MONTH);
			}
		}
		return StringPool.BLANK;
	}
	
	/**
	 * Given an Instant, it evaluates the friendlyDate following the pattern: if
	 * same year: dd MMMM if different year: dd MMMM YYYY
	 *
	 * The Instant is first parsed into the TimeZone and then compared to
	 * today's date evaluated in same timeZone
	 *
	 * @param instant the instant
	 * @param timeZoneId the time zone ID
	 * @return the friendly date, or empty string if the date is empty
	 */
	public String getFriendlyDate(Instant instant, String timeZoneId) {
		return getFriendlyDate(instant, timeZoneId, null);
	}

	/**
	 * Given an Instant, it evaluates the friendlyTime following the pattern:
	 * Today, Yesterday, This week, Last week, This month, February, January,
	 * December 2015, November 2015
	 *
	 * The Instant is first parsed into the TimeZone and then compared to
	 * today's date evaluated in same timeZone
	 *
	 * @param instant the instant
	 * @param timeZoneId the time zone ID
	 * @param locale the locale
	 * @return the friendly time, or empty string if the instant is empty
	 */
	public String getFriendlyTime(Instant instant, String timeZoneId, Locale locale) {
		if (Validator.isNotNull(instant)) {
			ResourceBundle bundle = localeMapUtil.getResourceBundle(locale);

			ZoneId zoneId = ZoneId.of(timeZoneId);
			LocalDateTime timeToCheck = LocalDateTime.ofInstant(instant, zoneId);
			LocalDateTime nowTime = LocalDateTime.now(zoneId);

			Period period = Period.between(timeToCheck.toLocalDate(), nowTime.toLocalDate());

			if (isDifferentYear(period, timeToCheck, nowTime)) {
				// Different year - Return Month and Year
				return getFormattedInstant(instant, timeZoneId, DatePatterns.MONTH_YEAR);
			} else if (isDifferentMonth(period, timeToCheck, nowTime)) {
				// Same year different month - Return Month
				return getFormattedInstant(instant, timeZoneId, DatePatterns.MONTH);
			} else if (isDifferentDay(period, timeToCheck, nowTime)) {
				// Same year same month but different day
				int daysDifference = period.getDays();
				if (daysDifference == 1) {
					// Only one day difference
					return bundle.getString("time-yesterday");
				} else if (daysDifference < 7) {
					// Same week
					return bundle.getString("time-this-week");
				} else {
					// Same month
					return bundle.getString("time-this-month");
				}
			} else {
				// No difference in year, month or day
				return bundle.getString("time-today");
			}
		}
		return StringPool.BLANK;
	}

	/**
	 * Converts the date into an Instant
	 *
	 * @param date the date
	 * @return null if the date is null, the Instant otherwise
	 */
	public Instant getInstantFromDate(Date date) {
		return Validator.isNotNull(date) ? date.toInstant() : null;
	}

	/**
	 * Converts the epochMillis into an Instant
	 *
	 * @param epochMillis the epoch in millis
	 * @return null if the epochMillis is null, the Instant otherwise
	 */
	public Instant getInstantFromMillis(Long epochMillis) {
		return Validator.isNotNull(epochMillis) ? Instant.ofEpochMilli(epochMillis) : null;
	}

	/**
	 * Converts the epochMillis into an Date
	 *
	 * @param epochMillis the epoch in millis
	 * @return Empty Optional if the epochMillis is null, the Date otherwise
	 */
	public Optional<Date> getDateFromMillis(Long epochMillis) {
		return Validator.isNotNull(epochMillis) ? Optional.of(new Date(epochMillis)) : Optional.empty();
	}

	/**
	 * Creates a future Instant that will never be reached
	 *
	 * @return Instant of now plus 30000 days
	 */
	public Instant getEndlessInstant() {
		return getNow().plus(30000, ChronoUnit.DAYS);
	}

	/**
	 * Returns the current time in UTC zone
	 *
	 * @return Instant of now
	 */
	public Instant getNow() {
		return Instant.now(Clock.system(ZONE_UTC));
	}

	/**
	 * Returns the current time in the specified timezone
	 *
	 * @param timeZoneId the timeZoneId
	 * @return the current instant in the timezone
	 */
	public Instant getNowForTimeZoneId(String timeZoneId) {
		ZoneId zoneId = ZoneId.of(timeZoneId);
		LocalDateTime nowTime = LocalDateTime.now(zoneId);
		return nowTime.toInstant(ZoneOffset.UTC);
	}

	private boolean isDifferentDay(Period period, LocalDateTime firstValue, LocalDateTime secondValue) {
		return period.getDays() != 0 || firstValue.getDayOfMonth() != secondValue.getDayOfMonth();
	}

	private boolean isDifferentMonth(Period period, LocalDateTime firstValue, LocalDateTime secondValue) {
		return period.getMonths() != 0 || firstValue.getMonthValue() != secondValue.getMonthValue();
	}

	private boolean isDifferentYear(Period period, LocalDateTime firstValue, LocalDateTime secondValue) {
		return period.getYears() != 0 || firstValue.getYear() != secondValue.getYear();
	}

	@Reference
	protected void setLocaleMapUtil(LocaleMapUtil localeMapUtil) {
		this.localeMapUtil = localeMapUtil;
	}

}
