/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.props;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;

@Component(immediate = true, service = PropsUtilHelper.class)
public class PropsUtilHelper {

	private static final Log LOG = LogFactoryUtil.getLog(PropsUtilHelper.class);
	
	public Boolean isCompanyDefaultCompany(String companyWebId) {
		return PropsUtil.get("company.default.web.id").equals(companyWebId);
	}
	
	public Boolean isDefaultCompanyAdminOnly() {
		return GetterUtil.getBoolean(PropsUtil.get("default.company.admin.only") ,false);
	}
	
	public Boolean canCustomiseCompany(String companyWebId) {
		boolean isCompanyDefaultCompany = isCompanyDefaultCompany(companyWebId);
		Boolean defaultCompanyAdminOnly = isDefaultCompanyAdminOnly();
		boolean result = !isCompanyDefaultCompany || !defaultCompanyAdminOnly;
		LOG.debug("Can customise company with webId: " + companyWebId + " = " + result + " - isCompanyDefaultCompany? "+isCompanyDefaultCompany + ", defaultCompanyAdminOnly? " + defaultCompanyAdminOnly);
		return result;
	}

}
