/**
 * Copyright (c) 2000-present PFI Knowledge Solutions Ltd. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
package com.pfiks.common.portal;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.util.PortalUtil;

@Component(immediate = true, service = PortalUtilHelper.class)
public class PortalUtilHelper {

	private static final Log LOG = LogFactoryUtil.getLog(PortalUtilHelper.class);

	/**
	 * Helper function to check User is company Admin, ignores exception
	 * 
	 * @param user the User Object
	 * @return true or false
	 */
	public boolean isCompanyAdmin(User user) {
		try {
			return PortalUtil.isCompanyAdmin(user);
		} catch (Exception e) {
			LOG.info(e);
			return false;
		}
	}
}